// babel.config.cjs
module.exports = {
  presets: [
    ['@babel/preset-env', { targets: { node: 'current' } }],
    '@babel/preset-typescript', // For TypeScript
    '@babel/preset-react' // For React JSX
  ],
  plugins: [
    '@babel/plugin-transform-runtime'
  ]
};
