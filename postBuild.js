import fs from 'fs';
import path from 'path';

function addJsExtension(dir) {
    const files = fs.readdirSync(dir);
    for (const file of files) {
        const fullPath = path.join(dir, file);
        if (fs.statSync(fullPath).isDirectory()) {
            addJsExtension(fullPath);
        } else if (fullPath.endsWith('.js')) {
            let content = fs.readFileSync(fullPath, 'utf8');
            // Adjusting regex to only target relative paths starting with './' or '../'
            content = content.replace(/from ['"](\..*?)['"];?/g, (match, p1) => {
                return p1.endsWith('.js') ? match : match.replace(p1, `${p1}.js`);
            });
            fs.writeFileSync(fullPath, content, 'utf8');
        }
    }
}

addJsExtension('./dist');
