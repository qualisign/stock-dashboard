
# Table of Contents

1.  [Quickstart](#orgf7a6b2b)
    1.  [Server Setup](#orgea4d873)
    2.  [Client Setup](#orgab5889c)
    3.  [Testing](#orgcf97f2f)
2.  [Tips for Using the Client](#org4afd889)
3.  [Design](#org9395148)
    1.  [Domain Language](#org5394ea8)
4.  [Domain Layer](#orgaae2864)
    1.  [Models](#org3d2745a)
        1.  [Trade](#orgddec6de)
        2.  [User](#orgf6e6728)
        3.  [Stock](#org20d5d11)
        4.  [Daily Stats](#org5689452)
    2.  [Services](#orgb3bb4a9)
        1.  [Realtime Service](#org82d1b0b)
        2.  [User Service](#org4755706)
        3.  [Watchlist Service](#org4e50054)
        4.  [Stock Service](#orgdfae7a0)
        5.  [DailyStats Service](#org8c22292)
5.  [Infrastructure Layer](#orgb9f7083)
    1.  [Communication](#org678697f)
        1.  [HTTP](#org5febd2c)
        2.  [WebSocket (WS)](#org0c352f4)
    2.  [Repositories](#org7a19842)
    3.  [Data](#org1c97505)
        1.  [Seeding](#orga752c54)
6.  [API Layer](#org9150c65)
    1.  [Routes](#orgfef1ff7)
    2.  [Middleware](#org2921cc0)
7.  [Client Layer](#orgd8c460c)
    1.  [Interface](#orgf783ffb)



<a id="orgf7a6b2b"></a>

# Quickstart


<a id="orgea4d873"></a>

## Server Setup

1.  Clone the repo
2.  `npm install` from project root
3.  `npm run start` from project root


<a id="orgab5889c"></a>

## Client Setup

1.  `cd /src/interface/web` from project root
2.  `npm install`
3.  `npm run start`
4.  If you'd like to run another client instance, open up a new shell process and repeat step 3 (and type "Y" to proceed with a new port)


<a id="orgcf97f2f"></a>

## Testing

-   I follow a mostly TDD approach to development, so much if not all the code is covered by tests. Due to time constraints, this process turned out to be messier than I expected, and when you run:
-   `npx run jest`
-   You will see some failing/outdated tests.
-   (I'd normally clean this up, but that's not the most important thing for improving the readability of this project given time constraints.)


<a id="org4afd889"></a>

# Tips for Using the Client

-   You must register and log in before doing any of the following
-   You must be subscribed to a stock in order to see updates


<a id="org9395148"></a>

# Design

I used DDD principles to organize this project. The idea is to make the code maintainable and organic.

Here's an example: repositories are used as an intermediary between databases and applications so that any storage strategies can be easily swapped (e.g., the app producer decides to use Dynamo instead of RDS, which in my view shouldn't be difficult, if DDD principles are adhered to).

This approach really shines when certain tables have very different constraints than others (such as scaling needs, read/write rates, etc). For example, stock prices are stored in an in-memory cache because 1) they are read/written rapidly 2) produce too much data to persist every change. This means the price seen by a user of a particular stock are simply the latest prices, retrieved from the cache, as a result of their GET request. The fact that these values are cached makes read access very fast, but can be demanding on storage. Since there is a predictable limit on the number of stocks tracked in this app (i.e., keys in the key-value store), the storage requirements will not be restrictive, so a cache can be benefited from.


<a id="org5394ea8"></a>

## Domain Language

An important concept in DDD is a domain language, which is just an agreed-upon way of referring to concepts important to the application's users and their goals. I've included the obvious concepts like *stock*, *trade*, and others in my domain model, which is a minimal representation of the important nouns in this application (from the users POV). The important verbs are also mostly obvious and ubiquitous (like *viewing* stock prices), but some may be less so (like *live-updating* stock prices). The point is that realtime communication is crucial to this application's appeal for its users, and this functionality therefore belongs to the domain layer.


<a id="orgaae2864"></a>

# Domain Layer


<a id="org3d2745a"></a>

## Models


<a id="orgddec6de"></a>

### Trade

Represents individual trade events with properties like event type, symbol, trade ID, exchange ID, price, size, timestamp, tape ID, and condition codes (optional).


<a id="orgf6e6728"></a>

### User

Captures essential user information necessary for authentication and identification within the system, including user ID, username, password hash, and email address.


<a id="org20d5d11"></a>

### Stock

Focuses on the key attributes relevant to stock trading, such as stock symbol and current price.


<a id="org5689452"></a>

### Daily Stats

Provides a comprehensive view of a stock's daily trading statistics, including high, low, high time, low time, and closing price.


<a id="orgb3bb4a9"></a>

## Services


<a id="org82d1b0b"></a>

### Realtime Service

Utilizes WebSocket technology for delivering real-time updates. Manages user subscriptions and notifications, handles events like client connections/disconnections, and broadcasts updates to subscribed users.


<a id="org4755706"></a>

### User Service

Implements user management functionalities essential for the application's authentication and user data handling processes.


<a id="org4e50054"></a>

### Watchlist Service

Manages user watchlists, integrating closely with realtime updates to provide customized data streams.


<a id="orgdfae7a0"></a>

### Stock Service

Manages stock data, facilitating operations such as adding, updating, or removing stock information.


<a id="org8c22292"></a>

### DailyStats Service

Handles management and updating of daily statistics for stocks, key to providing a comprehensive view of stock performance.


<a id="orgb9f7083"></a>

# Infrastructure Layer


<a id="org678697f"></a>

## Communication


<a id="org5febd2c"></a>

### HTTP

Handles RESTful requests for non-real-time operations such as fetching historical data or performing CRUD operations on user profiles and watchlists.


<a id="org0c352f4"></a>

### WebSocket (WS)

Facilitates real-time bidirectional communication between clients and the server. It is used primarily for the real-time trades and price updates feature, enabling the dynamic flow of information without the need to refresh the web interface.


<a id="org7a19842"></a>

## Repositories

Abstract interfaces are implemented to provide flexibility in swapping out data storage solutions without impacting the domain logic. Current implementations include in-memory and SQLite databases, which can be replaced as scalability needs evolve.


<a id="org1c97505"></a>

## Data


<a id="orga752c54"></a>

### Seeding

Initial mock data is generated through factories to simulate real-world trading scenarios, aiding in both development and testing phases. This is critical for ensuring the system can handle expected operational loads and data integrity is maintained.


<a id="org9150c65"></a>

# API Layer


<a id="orgfef1ff7"></a>

## Routes

Define endpoints for handling requests related to stocks, trades, user management, and more. Routing is carefully designed to reflect REST principles, ensuring intuitive and predictable API interfaces.


<a id="org2921cc0"></a>

## Middleware

Currently only used for streamlining HTTP authentication. Requires a jwt token of clients for the endpoints to which it is applied.

Ensures that requests are authenticated and properly authorized before accessing sensitive endpoints. This is crucial for maintaining security and integrity of user data.


<a id="orgd8c460c"></a>

# Client Layer


<a id="orgf783ffb"></a>

## Interface

Provides a responsive web interface designed for both desktop and mobile browsers, allowing users to interact seamlessly with the real-time features of the application.
The interface utilizes React for dynamic content updates, which is particularly effective in conjunction with WebSocket communication for live data feeds.

