module.exports = {
  // Entry point of your application
  entry: './src/index.js',

  // Output configuration
  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js'
  },

  // Module rules for processing files
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,  // Process both .js and .jsx files
        exclude: /node_modules/,  // Do not process files in the node_modules directory
        use: {
          loader: 'babel-loader',  // Use babel-loader to process these files
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']  // Presets used by Babel
          }
        }
      }
    ]
  }
};
