
# First, rename occurrences within files, excluding node_modules
find . -type f ! -path "*/node_modules/*" -exec grep -Il 'uplexStreamAdapter' {} \; | xargs sed -i '' 's/uplexStreamAdapter/uplexStreamAdapter/g'

# Then, rename the files themselves, excluding node_modules
find . -type f ! -path "*/node_modules/*" -name '*uplexStreamAdapter*' | while read fname; do
    mv "$fname" "$(echo $fname | sed 's/uplexStreamAdapter/uplexStreamAdapter/g')"
done
