import { Server as HttpServer } from 'http';
import { WebSocketServer } from 'ws';

describe('WebSocket connection test', () => {
  let server, wss;

  beforeAll(done => {
    server = new HttpServer();
    wss = new WebSocketServer({ noServer: true });

    server.on('upgrade', (request, socket, head) => {
      wss.handleUpgrade(request, socket, head, ws => {
        wss.emit('connection', ws, request);
      });
    });

    server.listen(0, () => {
      const port = server.address().port;
      console.log(`Server running on port ${port}`);
      done();
    });
  });

  afterAll(() => {
    wss.close();
    server.close();
  });

  test('WebSocket server should be created', () => {
    expect(wss).toBeDefined();
  });
});
