import request from 'supertest';
import express from 'express';
import { DailyStatsController } from '../../../src/interface/api/controllers/dailyStatsController';
import { IDailyStatsService } from '../../../src/domain/interfaces';

describe('DailyStatsController', () => {
    let app;
    let dailyStatsServiceMock;

    beforeEach(() => {
        app = express();
        dailyStatsServiceMock = {
            getDailyStats: jest.fn()
        };
        const controller = new DailyStatsController(dailyStatsServiceMock);
        app.use('/api/daily-stats', controller.router);
    });

    describe('GET /:symbol', () => {
        it('should return 400 if symbol is not provided', async () => {
            const response = await request(app).get('/api/daily-stats/');
expect(response.status).toBe(400);
	  expect(response.body).toEqual({ error: "Stock symbol is required" });
	  
        });

      it('should return 200 and the daily stats for the symbol', async () => {
            const mockStats = { symbol: 'AAPL', high: 150, low: 145, closing: 148 };
            dailyStatsServiceMock.getDailyStats.mockResolvedValue(mockStats);

            const response = await request(app).get('/api/daily-stats/AAPL');
            expect(response.status).toBe(200);
            expect(response.body).toEqual(mockStats);
        });

        it('should handle errors and return 500', async () => {
            dailyStatsServiceMock.getDailyStats.mockRejectedValue(new Error('Service failure'));

            const response = await request(app).get('/api/daily-stats/AAPL');

expect(response.status).toBe(500);
	  expect(response.body).toEqual({ error: "Failed to retrieve daily stats" });
	  
        });
    });
});
