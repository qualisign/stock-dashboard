import request from 'supertest';
import express from 'express';
import { StockController } from '../../../src/interface/api/controllers/stockController';

describe('StockController', () => {
    let app;
    let stockServiceMock;

    beforeEach(() => {
        app = express();
        app.use(express.json()); // Middleware to parse JSON bodies
        stockServiceMock = {
            upsertStock: jest.fn(),
            deleteStock: jest.fn()
        };
        const controller = new StockController(stockServiceMock);
        app.use('/api/stocks', controller.router);
    });

    describe('PUT /:symbol', () => {
        it('should upsert a stock and return success message', async () => {
            const symbol = 'AAPL';
            const price = 150.00;
            stockServiceMock.upsertStock.mockResolvedValue();

            const response = await request(app).put(`/api/stocks/${symbol}`).send({ price });
            expect(response.status).toBe(200);
            expect(response.body).toEqual({ success: true, message: 'Stock updated successfully', symbol, price });
        });

        it('should handle errors when upserting a stock', async () => {
            const symbol = 'AAPL';
            const price = 150.00;
            stockServiceMock.upsertStock.mockRejectedValue(new Error('Upsert error'));

            const response = await request(app).put(`/api/stocks/${symbol}`).send({ price });
            expect(response.status).toBe(500);
            expect(response.body).toEqual({ success: false, message: 'Upsert error' });
        });
    });

describe('DELETE /:symbol', () => {
    it('should delete a stock and return a 204 status', async () => {
        const symbol = 'AAPL';
        stockServiceMock.deleteStock.mockResolvedValue();

        const response = await request(app).delete(`/api/stocks/${symbol}`);
        expect(response.status).toBe(204);
        expect(response.text).toBe('');
    });

    it('should handle errors when deleting a stock', async () => {
        const symbol = 'AAPL';
        const errorMessage = 'Delete error';
        stockServiceMock.deleteStock.mockRejectedValue(new Error(errorMessage));

        const response = await request(app).delete(`/api/stocks/${symbol}`);
        expect(response.status).toBe(500);
        expect(response.body).toEqual({ success: false, message: errorMessage }); // Corrected to check response.body
    });
});

});
