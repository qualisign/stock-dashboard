import request from 'supertest';
import express from 'express';
import { Router } from 'express'; // Ensure Router is correctly imported
import { WatchlistController } from '../../../dist/interface/api/controllers/watchlistController';
import { IWatchlistService } from '../../../dist/domain/interfaces'; // Ensure this is correctly imported

describe('WatchlistController', () => {
    let app
    let watchlistServiceMock

    beforeEach(() => {
        app = express();
        app.use(express.json()); // Middleware to parse JSON bodies
        watchlistServiceMock = {
            getWatchlist: jest.fn(),
            upsertWatchlist: jest.fn(),
            deleteWatchlist: jest.fn()
        }; // Cast to IWatchlistService to satisfy TypeScript
        const controller = new WatchlistController(watchlistServiceMock);
        app.use('/api/watchlist', controller.router);
    });

    describe('GET /:userId', () => {
        it('should return the watchlist for a user', async () => {
            const userId = 'user1';
            const expectedWatchlist = ['AAPL', 'GOOGL'];
            watchlistServiceMock.getWatchlist.mockResolvedValue(expectedWatchlist);

            const response = await request(app).get(`/api/watchlist/${userId}`);
            expect(response.status).toBe(200);
            expect(response.body).toEqual(expectedWatchlist);
        });

        it('should handle errors when getting the watchlist', async () => {
            watchlistServiceMock.getWatchlist.mockRejectedValue(new Error('Service error'));
            const response = await request(app).get('/api/watchlist/user1');
            expect(response.status).toBe(500);
            expect(response.text).toBe('Service error');
        });
    });

    describe('PUT /:userId', () => {
        it('should update the watchlist for a user', async () => {
            const userId = 'user2';
            const symbols = ['MSFT', 'FB'];
            watchlistServiceMock.upsertWatchlist.mockResolvedValue();

            const response = await request(app).put(`/api/watchlist/${userId}`).send({ stocks: symbols });
            expect(response.status).toBe(200);
            expect(response.text).toBe('Watchlist updated successfully');
        });

        it('should handle errors when updating the watchlist', async () => {
            watchlistServiceMock.upsertWatchlist.mockRejectedValue(new Error('Update error'));
            const response = await request(app).put('/api/watchlist/user2').send({ stocks: ['MSFT', 'FB'] });
            expect(response.status).toBe(500);
            expect(response.text).toBe('Update error');
        });
    });

    describe('DELETE /:userId', () => {
        it('should delete the watchlist for a user', async () => {
            const userId = 'user3';
            watchlistServiceMock.deleteWatchlist.mockResolvedValue();

            const response = await request(app).delete(`/api/watchlist/${userId}`);
            expect(response.status).toBe(204);
            expect(response.text).toBe('');
        });

        it('should handle errors when deleting the watchlist', async () => {
            watchlistServiceMock.deleteWatchlist.mockRejectedValue(new Error('Delete error'));
            const response = await request(app).delete('/api/watchlist/user3');
            expect(response.status).toBe(500);
            expect(response.text).toBe('Delete error');
        });
    });
});
