import request from 'supertest';
import express from 'express';
import { UserController } from '../../../src/interface/api/controllers/userController';
import { IUserService } from '../../../src/domain/interfaces';

const authServiceMock = {
  validateUser: jest.fn(),
  generateToken: jest.fn(),
  verifyToken: jest.fn()
};


jest.mock('../../../src/domain/services/authService', () => {
  return {
    AuthService: jest.fn().mockImplementation(() => ({
      validateUser: jest.fn().mockResolvedValue('someToken'), // Adjust according to your test needs
      generateToken: jest.fn(),
      verifyToken: jest.fn()
    }))
  };
});



describe('UserController', () => {
  let app;
  let userServiceMock;

  beforeEach(() => {
    app = express();
    app.use(express.json());
    userServiceMock = {
      login: jest.fn(),
      createUser: jest.fn(),
      deleteUser: jest.fn(),
      findByUsername: jest.fn()
    };
    // Instantiate UserController with the mocked AuthService
    const controller = new UserController(userServiceMock, authServiceMock);
    app.use('/api/users', controller.router);
  });


  describe('POST /login', () => {

    it('should login successfully if credentials are correct', async () => {
    userServiceMock.findByUsername.mockResolvedValue({ id: 'user123', username: 'user' });
    authServiceMock.validateUser.mockResolvedValue('someToken');
    const userCredentials = { username: 'user', password: 'pass' };

    const response = await request(app).post('/api/users/login').send(userCredentials);
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ success: true, token: 'someToken' });
    });




it('should return failure if login is unsuccessful', async () => {
    const userCredentials = { username: 'user', password: 'wrongpass' };
    userServiceMock.findByUsername.mockResolvedValue({ id: 'user123', username: 'user' });
    authServiceMock.validateUser.mockResolvedValue(null);

    const response = await request(app).post('/api/users/login').send(userCredentials);
    expect(response.status).toBe(401);
    expect(response.body).toEqual({ success: false, message: "Authentication failed" });
});


    it('should handle errors during login', async () => {
  userServiceMock.findByUsername.mockImplementation(() => {
    throw new Error('Unexpected database error');
  });
  const response = await request(app).post('/api/users/login').send({ username: 'user', password: 'pass' });
  expect(response.status).toBe(500);
  expect(response.body).toEqual({ success: false, message: "Internal server error" });
    });


  });

  describe('POST /register', () => {
    it('should register a user successfully', async () => {
      const newUser = { username: 'newuser', password: 'newpass', email: 'new@example.com' };
      userServiceMock.createUser.mockResolvedValue('123');

      const response = await request(app).post('/api/users/register').send(newUser);
      expect(response.status).toBe(201);
      expect(response.body).toEqual({ userId: '123' });
    });

    it('should handle errors during registration', async () => {
      userServiceMock.createUser.mockRejectedValue(new Error('Registration failed'));
      const response = await request(app).post('/api/users/register').send({ username: 'test', password: 'test', email: 'test@example.com' });
      expect(response.status).toBe(500);
      expect(response.body).toEqual({ message: "Registration failed" });

    });
  });

  describe('DELETE /:userId', () => {
    it('should delete a user successfully', async () => {
      userServiceMock.deleteUser.mockResolvedValue();
      const response = await request(app).delete('/api/users/1');
      expect(response.status).toBe(204);
      expect(response.text).toBe('');
    });

    it('should handle errors during user deletion', async () => {
      userServiceMock.deleteUser.mockRejectedValue(new Error('Deletion failed'));
      const response = await request(app).delete('/api/users/1');
      expect(response.status).toBe(500);
      expect(response.body).toEqual({ message: "Deletion failed" });
      
    });
  });
});
