import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import WS from 'jest-websocket-mock';
import StockDashboard from '../../../dist/interface/web/src/components/stockDashboard';  

describe('StockDashboard', () => {
    let server;
    const port = 8080;  // Ensure this is the port you want to test against

    beforeEach(async () => {
        server = new WS(`ws://localhost:${port}`);
    });

    afterEach(() => {
        WS.clean();
    });

    it('should establish a websocket connection and receive data', async () => {
        render(<StockDashboard port={port} />);

        await server.connected;
        const stockData = { symbol: "AAPL", price: 150 };
        server.send(JSON.stringify(stockData));
        
        await waitFor(() => screen.getByText(`Symbol: AAPL, Price: 150`));

        expect(screen.getByText(`Symbol: AAPL, Price: 150`)).toBeInTheDocument();
    });

    it('should handle websocket connection closing', async () => {
        const { getByText } = render(<StockDashboard port={port} />);
        
        await server.connected;
        WS.clean();  // This will close the connection
        
        await waitFor(() => getByText('WebSocket connection closed'));
        
        expect(getByText('WebSocket connection closed')).toBeInTheDocument();
    });
});
