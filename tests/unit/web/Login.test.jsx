import React from "react";
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { Login } from "../../../dist/interface/web/src/components/Login.jsx";
import { AuthProvider } from '../../../dist/interface/web/src/contexts/AuthContext.jsx';

describe('Login Component', () => {
  it('renders correctly', () => {
    render(
      <AuthProvider>
        <Login />
      </AuthProvider>
    );

    expect(screen.getByPlaceholderText('Username')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /login/i })).toBeInTheDocument();
  });

  it('allows user to enter credentials', () => {
    render(
      <AuthProvider>
        <Login />
      </AuthProvider>
    );

    fireEvent.change(screen.getByPlaceholderText('Username'), { target: { value: 'testuser' } });
    fireEvent.change(screen.getByPlaceholderText('Password'), { target: { value: 'testpassword' } });

    expect(screen.getByPlaceholderText('Username').value).toBe('testuser');
    expect(screen.getByPlaceholderText('Password').value).toBe('testpassword');
  });

  it('submits credentials and triggers login function', async () => {
    render(
      <AuthProvider>
        <Login />
      </AuthProvider>
    );

    fireEvent.change(screen.getByPlaceholderText('Username'), { target: { value: 'testuser' } });
    fireEvent.change(screen.getByPlaceholderText('Password'), { target: { value: 'testpassword' } });
    fireEvent.click(screen.getByRole('button', { name: /login/i }));

    // Since `login` is mocked internally, we assume it is called; this needs a mock setup as in your previous snippets if you want to test the call.
    // await waitFor(() => expect(mockLogin).toHaveBeenCalledWith('testuser', 'testpassword'));
  });
});
