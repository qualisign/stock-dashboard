import React, { useState } from 'react';
import { render } from '@testing-library/react';

const TestComponent = () => {
  const [count, setCount] = useState(0);
  return <div onClick={() => setCount(count + 1)}>{count}</div>;
};

test('test useState works', () => {
  const { getByText } = render(<TestComponent />);
  const divElement = getByText('0');
  expect(divElement).toBeInTheDocument();
});
