// SqliteDailyStatsRepository.test.js
import { SqliteDailyStatsRepository } from '../../../src/infrastructure/data/repositories/sqliteDailyStatsRepository';
import sqlite3 from 'sqlite3';

describe('SqliteDailyStatsRepository', () => {
    let repo;

    beforeEach(async () => {
        repo = new SqliteDailyStatsRepository(':memory:');
        await new Promise((resolve, reject) => {
            repo.db.run(`
                CREATE TABLE IF NOT EXISTS daily_stats (
                    symbol TEXT PRIMARY KEY,
                    high REAL,
                    highDateTime TEXT,
                    low REAL,
                    lowDateTime TEXT,
                    closing REAL
                );`, (err) => {
                if (err) reject(err);
                else resolve();
            });
        });
    });

    afterEach(() => {
        repo.close();
    });

    describe('getDailyStats', () => {
        it('should resolve with the daily stats if found', async () => {
            const expectedStats = { symbol: 'AAPL', high: 150, highDateTime: '2022-04-25', low: 145, lowDateTime: '2022-04-25', closing: 148 };
            await repo.upsertDailyStats(expectedStats);

            const result = await repo.getDailyStats('AAPL');
            expect(result).toEqual(expectedStats);
        });

        it('should resolve with null if no stats are found', async () => {
            const result = await repo.getDailyStats('AAPL');
            expect(result).toBeNull();
        });
    });

    describe('upsertDailyStats', () => {
        it('should resolve without errors when stats are inserted or updated successfully', async () => {
            await expect(repo.upsertDailyStats({
                symbol: 'AAPL', high: 155, highDateTime: '2022-04-26', low: 150, lowDateTime: '2022-04-26', closing: 153
            })).resolves.toBeUndefined();
        });
    });

    describe('deleteDailyStats', () => {
        it('should resolve without errors when stats are deleted successfully', async () => {
            await repo.upsertDailyStats({ symbol: 'AAPL', high: 155, highDateTime: '2022-04-26', low: 150, lowDateTime: '2022-04-26', closing: 153 });
            await expect(repo.deleteDailyStats('AAPL')).resolves.toBeUndefined();
        });
    });
});
