import { SqliteUserRepository } from '../../../src/infrastructure/data/repositories/sqliteUserRepository';
import sqlite3 from 'sqlite3';

describe('SqliteUserRepository', () => {
    let repo;

    beforeEach(async () => {
        repo = new SqliteUserRepository(':memory:');
        // Ensuring the setup completes by running a dummy query that forces waiting for the table to exist
        await new Promise((resolve, reject) => {
            repo.db.run("SELECT name FROM sqlite_master WHERE type='table' AND name='users';", (err, row) => {
                if (err) reject(err);
                else resolve(row); // This ensures that the table check has been completed
            });
        });
    });

    afterEach(async () => {
        await new Promise((resolve, reject) => {
            repo.db.close((err) => {
                if (err) reject(err);
                else resolve();
            });
        });
    });
  
  describe('findByUsername', () => {
    it('should return the user if found', async () => {
            const user = { id: '1', username: 'testuser', passwordHash: 'hashedpassword', email: 'test@example.com' };
            await repo.createUser(user);
            const fetchedUser = await repo.findByUsername('testuser');
            expect(fetchedUser).toEqual(user);
        });

        it('should return null if user is not found', async () => {
            const fetchedUser = await repo.findByUsername('nonexistent');
            expect(fetchedUser).toBeNull();
        });
    });

    describe('createUser', () => {
        it('should create a user successfully', async () => {
            const user = { id: '1', username: 'newuser', passwordHash: 'hashedpassword', email: 'new@example.com' };
            const userId = await repo.createUser(user);
            expect(userId).toBe('1');
            const fetchedUser = await repo.findByUsername('newuser');
            expect(fetchedUser).toEqual(user);
        });
    });

    describe('updateUser', () => {
        it('should update the user details', async () => {
            const user = { id: '1', username: 'updateuser', passwordHash: 'hashedpassword', email: 'update@example.com' };
            await repo.createUser(user);
            user.email = 'updated@example.com';
            await repo.updateUser('1', user);
            const updatedUser = await repo.findByUsername('updateuser');
            expect(updatedUser.email).toBe('updated@example.com');
        });
    });

    describe('deleteUser', () => {
        it('should delete the user', async () => {
            const user = { id: '1', username: 'deleteuser', passwordHash: 'hashedpassword', email: 'delete@example.com' };
            await repo.createUser(user);
            await repo.deleteUser('1');
            const fetchedUser = await repo.findByUsername('deleteuser');
            expect(fetchedUser).toBeNull();
        });
    });
});
