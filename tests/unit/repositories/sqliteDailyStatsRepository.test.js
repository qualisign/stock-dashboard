// SqliteDailyStatsRepository.test.js
import { SqliteDailyStatsRepository } from '../../../src/infrastructure/data/repositories/sqliteDailyStatsRepository';
import sqlite3 from 'sqlite3';
import { DailyStats } from '../../../src/domain/models/dailyStats';

describe('SqliteDailyStatsRepository', () => {
    let repo;

beforeEach(async () => {
    repo = new SqliteDailyStatsRepository(":memory:");
    await repo.setupDatabase();
    // Optionally check that the table exists to ensure readiness
    await new Promise((resolve, reject) => {
        repo.db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='dailyStats';", (err, row) => {
            if (err || !row) reject('Table not found');
            resolve();
        });
    });
});


    afterEach(() => {
        // Close the database connection after each test
        repo.db.close();
    });

    describe('getDailyStats', () => {
        it('should resolve with the daily stats if found', async () => {
            const stats = {
                symbol: 'AAPL',
                high: 150,
                highDateTime: '2022-04-25',
                low: 145,
                lowDateTime: '2022-04-25',
                closing: 148
            };
            await repo.upsertDailyStats(stats);

            const result = await repo.getDailyStats('AAPL');
            expect(result).toEqual(stats);
        });

        it('should resolve with null if no stats are found', async () => {
            const result = await repo.getDailyStats('AAPL');
            expect(result).toBeNull();
        });
    });

    describe('upsertDailyStats', () => {
        it('should resolve without errors when stats are inserted or updated successfully', async () => {
            const stats = {
                symbol: 'AAPL',
                high: 155,
                highDateTime: '2022-04-26',
                low: 150,
                lowDateTime: '2022-04-26',
                closing: 153
            };
            await expect(repo.upsertDailyStats(stats)).resolves.toBeUndefined();
        });
    });

    describe('deleteDailyStats', () => {
        it('should resolve without errors when stats are deleted successfully', async () => {
            const symbol = 'AAPL';
            await repo.upsertDailyStats({
                symbol,
                high: 155,
                highDateTime: '2022-04-26',
                low: 150,
                lowDateTime: '2022-04-26',
                closing: 153
            });
            await expect(repo.deleteDailyStats(symbol)).resolves.toBeUndefined();
        });
    });
});
