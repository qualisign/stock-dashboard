// Import necessary modules and interfaces
import { ApplicationService } from '../../../src/application/applicationService';
import { IUserService, IRealtimeService } from '../../../src/domain/interfaces';

describe('ApplicationService', () => {
  let userServiceMock;
  let authServiceMock;
    let realtimeServiceMock;
    let appService;

    // Setup console mocking before all tests
    beforeAll(() => {
        jest.spyOn(console, 'log').mockImplementation(() => {});
        jest.spyOn(console, 'error').mockImplementation(() => {});
    });

    // Restore console methods after all tests
    afterAll(() => {
        console.log.mockRestore();
        console.error.mockRestore();
    });

    // Reset mocks and create service instances before each test
  beforeEach(() => {

    authServiceMock = {
      validateUser: jest.fn().mockResolvedValue(true), // Adjust based on expected behavior
      generateToken: jest.fn().mockResolvedValue("someToken")
    };

    
    userServiceMock = {
      findByUsername: jest.fn().mockResolvedValue({ id: 'user123', username: 'validUser' }), // Default to a found user
      getUserId: jest.fn(),
      login: jest.fn(),
      logout: jest.fn(),
      createUser: jest.fn().mockResolvedValue('userId123'), // Assume successful user creation
    };

    realtimeServiceMock = {
        startStreamingForUser: jest.fn().mockResolvedValue(),
        stopStreamingForUser: jest.fn().mockResolvedValue(),
        adjustSubscriptions: jest.fn(),
        watchlistService: {
            getWatchlist: jest.fn().mockResolvedValue(['AAPL', 'MSFT']),
            upsertWatchlist: jest.fn().mockResolvedValue(),
            deleteWatchlist: jest.fn().mockResolvedValue(),
        },
        unsubscribeAll: jest.fn().mockResolvedValue(),
    };

    appService = new ApplicationService(userServiceMock, realtimeServiceMock);
});


describe('loginUser', () => {
    it('should authenticate and start streaming if credentials are valid', async () => {
        userServiceMock.findByUsername.mockResolvedValue({ id: 'user123', username: 'validUser' });
        const isAuthenticated = true; // Simulate authentication success
        await expect(appService.loginUser('validUser', 'validPassword')).resolves.not.toThrow();
        expect(realtimeServiceMock.startStreamingForUser).toHaveBeenCalledWith('user123');
    });

    it('should not start streaming if credentials are invalid', async () => {
        userServiceMock.findByUsername.mockResolvedValue(null); // Simulate user not found
        await expect(appService.loginUser('invalidUser', 'password')).rejects.toThrow("User not found.");
        expect(realtimeServiceMock.startStreamingForUser).not.toHaveBeenCalled();
    });
});


  describe('logoutUser', () => {
    it('should log out user and unsubscribe from all subscriptions', async () => {
            await appService.logoutUser('user123');

            expect(userServiceMock.logout).toHaveBeenCalledWith('user123');
            expect(realtimeServiceMock.unsubscribeAll).toHaveBeenCalledWith('user123');
        });
    });

    describe('upsertWatchlist', () => {
        it('should upsert the watchlist and adjust subscriptions', async () => {
            await appService.upsertWatchlist('user123', ['AAPL', 'MSFT']);

            expect(realtimeServiceMock.watchlistService.upsertWatchlist).toHaveBeenCalledWith('user123', ['AAPL', 'MSFT']);
            expect(realtimeServiceMock.adjustSubscriptions).toHaveBeenCalledWith('user123', ['AAPL', 'MSFT']);
        });
    });

    describe('removeWatchlist', () => {
        it('should delete the watchlist and clear subscriptions', async () => {
            await appService.removeWatchlist('user123');

            expect(realtimeServiceMock.watchlistService.deleteWatchlist).toHaveBeenCalledWith('user123');
            expect(realtimeServiceMock.adjustSubscriptions).toHaveBeenCalledWith('user123', []);
        });
    });

  describe('streaming', () => {
    it('should start streaming for a valid user', async () => {
    userServiceMock.findByUsername.mockResolvedValue({ id: 'user123', username: 'validUser' });
    userServiceMock.login.mockResolvedValue(true);
    realtimeServiceMock.startStreamingForUser.mockResolvedValue();

    await appService.startStreamingForUser('validUser', 'validPassword');

    expect(realtimeServiceMock.startStreamingForUser).toHaveBeenCalledWith('user123');
    expect(realtimeServiceMock.adjustSubscriptions).toHaveBeenCalled();
});

it('should stop streaming for a user', async () => {
    await appService.stopStreamingForUser('user123');

    expect(realtimeServiceMock.stopStreamingForUser).toHaveBeenCalledWith('user123');
    expect(userServiceMock.logout).toHaveBeenCalledWith('user123');
});
});


  });
