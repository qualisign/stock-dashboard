import { DailyStatsService } from '../../../src/domain/services/dailyStatsService';
import { IDailyStatsRepository } from '../../../src/infrastructure/data/repositories/IDailyStatsRepository';
import { TradeEvent } from '../../../src/domain/models/tradeEvent';

describe('DailyStatsService', () => {
  let dailyStatsService;
  let mockRepository;

  beforeEach(() => {
    mockRepository = {
      getDailyStats: jest.fn(),
      upsertDailyStats: jest.fn()
    };
    dailyStatsService = new DailyStatsService(mockRepository);
  });

  describe('handleStockUpdates', () => {
    it('should update the cache and database with new stats from trade events', async () => {
      const events = [
        { sym: 'AAPL', p: 150 },
        { sym: 'AAPL', p: 155 },
        { sym: 'AAPL', p: 145 }
      ];

      mockRepository.upsertDailyStats.mockResolvedValue(); // Ensure this is mocked to resolve

      await dailyStatsService.handleStockUpdates(events);

      const updatedStats = await dailyStatsService.getDailyStats('AAPL');
      expect(updatedStats.high).toEqual(155);
      expect(updatedStats.low).toEqual(145);
      expect(updatedStats.closing).toEqual(145); // Last price from the events
      expect(mockRepository.upsertDailyStats).toHaveBeenCalledWith(expect.objectContaining({
	symbol: 'AAPL',
	high: expect.any(Number),
	low: expect.any(Number),
	closing: expect.any(Number),
	highDateTime: expect.any(Date),
	lowDateTime: expect.any(Date)
      }));
    });
  });

  describe('updateDailyStats', () => {
    it('should call the repository to update daily stats', async () => {
      const stockSymbol = 'AAPL';
      const stats = { volume: 2000, price: 155, high: 155, low: 150, closing: 155 };

      await dailyStatsService.updateDatabaseWithStats(stats);

      expect(mockRepository.upsertDailyStats).toHaveBeenCalledWith(stats);
    });

    it('should handle errors during the update operation', async () => {
      const stockSymbol = 'AAPL';
      const stats = { volume: 2000, price: 155, high: 155, low: 150, closing: 155 };
      const errorMessage = 'Database error';

      mockRepository.upsertDailyStats.mockRejectedValue(new Error(errorMessage));

      await expect(dailyStatsService.updateDatabaseWithStats(stats)).rejects.toThrow(errorMessage);
    });
  });
});
