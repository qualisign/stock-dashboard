const { UserService } = require('../../../src/domain/services/userService');
const { IUserRepository } = require('../../../src/infrastructure/data/repositories/IUserRepository');
const bcrypt = require('bcrypt');

describe('UserService', () => {
    let userService;
    let mockUserRepository;

    beforeEach(() => {
        mockUserRepository = {
            findByUsername: jest.fn(),
            createUser: jest.fn(),
            deleteUser: jest.fn(),
        };
        userService = new UserService(mockUserRepository);
    });

    it('should authenticate a user with valid credentials', async () => {
        const username = 'testUser';
        const password = 'testPass';
        const user = { id: '1', username, passwordHash: await bcrypt.hash(password, 10) };
        mockUserRepository.findByUsername.mockResolvedValue(user);

        const result = await userService.login(username, password);
        expect(result).toBeTruthy();
    });

  it('should not authenticate a user with invalid credentials', async () => {
    const username = 'testUser';
    const password = 'testPass';
    mockUserRepository.findByUsername.mockResolvedValue(null);

    const result = await userService.login(username, password);
    expect(result).toBeFalsy();
  });



    it('should create a user successfully', async () => {
        const username = 'newUser';
        const password = 'newPass';
        const email = 'new@example.com';
        const userId = '2';
        mockUserRepository.createUser.mockResolvedValue(userId);

        const result = await userService.createUser(username, password, email);
        expect(result).toBe(userId);
    });
});
