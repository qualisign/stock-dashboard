import { StockService } from '../../../src/domain/services/StockService';
import { IStockRepository } from '../../../src/infrastructure/data/repositories/IStockRepository';

describe('StockService', () => {
  let stockService;
  let mockStockRepository;

  beforeEach(() => {
    mockStockRepository = {
      upsertStock: jest.fn(),
      deleteStock: jest.fn(),
    };
    stockService = new StockService(mockStockRepository);
  });

  describe('upsertStock', () => {
    it('should successfully upsert a stock', async () => {
      const symbol = 'AAPL';
      const price = 150;
      mockStockRepository.upsertStock.mockResolvedValue();

      await stockService.upsertStock(symbol, price);

      expect(mockStockRepository.upsertStock).toHaveBeenCalledWith(symbol, price);
    });

    it('should throw an error if the repository throws during upsert', async () => {
      const symbol = 'AAPL';
      const price = 150;
      const errorMessage = 'Database error during upsert';
      mockStockRepository.upsertStock.mockRejectedValue(new Error(errorMessage));

      await expect(stockService.upsertStock(symbol, price)).rejects.toThrow(`Failed to upsert stock ${symbol} at price ${price}: ${errorMessage}`);
    });
  });

  describe('deleteStock', () => {
    it('should delete a stock given its symbol', async () => {
      const symbol = 'AAPL';
      mockStockRepository.deleteStock.mockResolvedValue();

      await stockService.deleteStock(symbol);

      expect(mockStockRepository.deleteStock).toHaveBeenCalledWith(symbol);
    });

    it('should throw an error if the repository throws during delete', async () => {
      const symbol = 'AAPL';
      const errorMessage = 'Database error during delete';
      mockStockRepository.deleteStock.mockRejectedValue(new Error(errorMessage));

      await expect(stockService.deleteStock(symbol)).rejects.toThrow(`Failed to delete stock ${symbol}: ${errorMessage}`);
    });
  });
});
