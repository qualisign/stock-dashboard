import { Server as HttpServer } from 'http';
import { Server as SocketIOServer } from "socket.io";
import ClientIO from 'socket.io-client';
import { RealtimeService } from '../../../src/domain/services/realtimeService'; 
import { DuplexStreamAdapter } from "../../../src/infrastructure/communication/adapters/duplexStreamAdapter";
import { MockReadStreamAdapter } from "../../../src/infrastructure/communication/adapters/MockReadStreamAdapter";

describe('RealtimeService with Socket.IO', () => {
  let httpServer, ioServer, clientSocket, realtimeService, mockStockService, mockReadStreamAdapter, port;

  beforeEach((done) => {
    httpServer = new HttpServer();
    httpServer.listen(() => { // Listen on a random free port
      port = httpServer.address().port;
      ioServer = new SocketIOServer(httpServer, {
        cors: {
          origin: '*',
          methods: ["GET", "POST"]
        }
      });

      mockReadStreamAdapter = new MockReadStreamAdapter();
      mockReadStreamAdapter.initializeDataStream(); // Start emitting mock data

      const authServiceMock = {
        verifyToken: jest.fn().mockImplementation(token => {
          if (!token) return Promise.resolve(null); // Handle empty or undefined tokens
          return Promise.resolve({ id: 'mockUserId', username: 'mockUser' });
        })
      };

      mockStockService = { upsertStock: jest.fn().mockResolvedValue() };

      realtimeService = new RealtimeService(
        new DuplexStreamAdapter(mockReadStreamAdapter, httpServer, authServiceMock),
        {}, // Dummy DailyStatsService
        {}, // Dummy WatchlistService
        mockStockService
      );

      clientSocket = ClientIO(`http://localhost:${port}`, {
        transports: ['websocket'],
        query: {
          token: 'mockToken'
        }
      });

      clientSocket.on('connect', done);
    });
  });

  afterEach((done) => {
    if (clientSocket.connected) {
      clientSocket.disconnect();
    }
    ioServer.close(() => {
      httpServer.close(() => {
        mockReadStreamAdapter.closeDataStream(); // Make sure to close the mock data stream
        done();
      });
    });
  });

  it('should update stock prices based on trade events', (done) => {
    const tradeEvents = [{ sym: 'AAPL', p: 149 }];
    // Wait for initial data to be handled
    setTimeout(() => {
      // Simulate receiving a trade event
      realtimeService.websocketAdapter.emit('tradeEvent', tradeEvents);
      expect(mockStockService.upsertStock).toHaveBeenCalledWith('AAPL', 149);
      done();
    }, 1000); // Ensure mock data emits at least once
  });
});
