import { WatchlistService } from '../../../src/domain/services/watchlistService';
import { IWatchlistRepository } from '../../../src/infrastructure/data/repositories/IWatchlistRepository';

describe('WatchlistService', () => {
  let watchlistService;
  let mockRepository;

  beforeEach(() => {
    mockRepository = {
      getWatchlistByUserId: jest.fn(),
      upsertWatchlist: jest.fn(),
      deleteWatchlistByUserId: jest.fn()
    };
    watchlistService = new WatchlistService(mockRepository);
  });

  describe('getWatchlist', () => {
    it('should return a list of watchlist items when the repository returns data', async () => {
      const userId = 'user1';
      const expectedWatchlist = ['AAPL', 'GOOGL'];
      mockRepository.getWatchlistByUserId.mockResolvedValue(expectedWatchlist);

      const result = await watchlistService.getWatchlist(userId);

      expect(result).toEqual(expectedWatchlist);
      expect(mockRepository.getWatchlistByUserId).toHaveBeenCalledWith(userId);
    });

    it('should throw an error when the repository throws', async () => {
      const userId = 'user1';
      mockRepository.getWatchlistByUserId.mockRejectedValue(new Error('Database error'));

      await expect(watchlistService.getWatchlist(userId)).rejects.toThrow('Failed to retrieve watchlist for user ID user1: Database error');
    });
  });

  describe('upsertWatchlist', () => {
    it('should call the repository to upsert a watchlist', async () => {
      const userId = 'user1';
      const symbols = ['MSFT', 'AMZN'];

      mockRepository.upsertWatchlist.mockResolvedValue(undefined); // Assume void return on success

      await watchlistService.upsertWatchlist(userId, symbols);

      expect(mockRepository.upsertWatchlist).toHaveBeenCalledWith(userId, symbols);
    });

    it('should handle errors during upsert operation', async () => {
      const userId = 'user1';
      const symbols = ['MSFT', 'AMZN'];
      mockRepository.upsertWatchlist.mockRejectedValue(new Error('Database error'));

      await expect(watchlistService.upsertWatchlist(userId, symbols)).rejects.toThrow('Failed to upsert watchlist for user ID user1: Database error');
    });
  });

  describe('deleteWatchlist', () => {
    it('should delete a watchlist for a user', async () => {
      const userId = 'user1';

      mockRepository.deleteWatchlistByUserId.mockResolvedValue(undefined); // Assume void return on success

      await watchlistService.deleteWatchlist(userId);

      expect(mockRepository.deleteWatchlistByUserId).toHaveBeenCalledWith(userId);
    });

    it('should handle errors during delete operation', async () => {
      const userId = 'user1';
      mockRepository.deleteWatchlistByUserId.mockRejectedValue(new Error('Database error'));

      await expect(watchlistService.deleteWatchlist(userId)).rejects.toThrow('Failed to delete watchlist for user ID user1: Database error');
    });
  });
});
