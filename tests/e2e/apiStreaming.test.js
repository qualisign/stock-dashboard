// Ensure you import SocketIOServer correctly
import { Server as SocketIOServer } from 'socket.io';
import http from 'http';
import { app } from '../../dist/index'; 
import io from 'socket.io-client';

jest.setTimeout(10000); 

describe('Polygon Data Streaming E2E Test', () => {
  let httpServer, ioServer, clientSocket;

  beforeAll(done => {
    httpServer = http.createServer(app);
    ioServer = new SocketIOServer(httpServer);

    httpServer.listen(0, () => {
      const port = httpServer.address().port;
      console.log(`Test server running on http://localhost:${port}`);
      clientSocket = io(`http://localhost:${port}`);
      clientSocket.on('connect', done);
    });
  });

  afterAll((done) => {
    if (clientSocket) clientSocket.close();
if (ioServer) ioServer.close(done);
    if (httpServer) httpServer.close();

  });

  test('should receive trade event data via WebSocket', done => {
    clientSocket.on('tradeEvent', (data) => {
      try {
        expect(data).toHaveProperty('sym');
        expect(data.sym).toBe('AAPL');
        done();
      } catch (error) {
        done(error);
      }
    });

    ioServer.emit('tradeEvent', { sym: 'AAPL', price: 150 });
  });
});

