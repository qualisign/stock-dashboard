const cucumber = require('@klaveness/cypress-cucumber-preprocessor').default;
const { startDevServer } = require('@cypress/webpack-dev-server');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackConfig = require('../../webpack.config'); // Your webpack configuration file for React

module.exports = (on, config) => {
  on('file:preprocessor', cucumber());

  on('dev-server:start', (options) =>
    startDevServer({
      options,
      webpackConfig: {
        ...webpackConfig,
        plugins: [...webpackConfig.plugins, new HtmlWebpackPlugin()]
      }
    })
  );

  return config;
};
