describe('Polygon Data Stream to Frontend', () => {
  let token;

  before(() => {
    cy.visit('http://localhost:3000'); // Adjust the URL to point to your local or test environment

    // Simulate user login and store the token
    cy.request('POST', 'http://localhost:3000/api/users/login', {
      username: 'testuser',
      password: 'testpassword'
    }).then((response) => {
      expect(response.body).to.have.property('token'); // Ensure the response has a token
      token = response.body.token;
    });
  });

  it('receives and displays data from Polygon after user login', () => {
    // Open WebSocket connection with authentication
    cy.window().then((win) => {
      const WebSocket = win.WebSocket;
      const ws = new WebSocket(`ws://localhost:8080?token=${token}`);

      // Listen for messages
      ws.onmessage = (event) => {
        const data = JSON.parse(event.data);
        expect(data).to.have.property('symbol');
        expect(data.symbol).to.equal('AAPL');

        // Display data in a component
        cy.get('.stock-list').should('contain', 'AAPL');
      };

      // Simulate server sending data
      ws.onopen = () => {
        ws.send(JSON.stringify({
          symbol: 'AAPL',
          price: 150.5
        }));
      };

      // Close WebSocket after the test
      cy.on('window:before:unload', () => {
        ws.close();
      });
    });
  });

  after(() => {
    // Optional: any cleanup if necessary
  });
});
