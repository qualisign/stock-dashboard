Feature: Real-Time Stock Price Updates

  As a logged-in stock trader
  I want to see real-time updates for stocks in my watchlist on my dashboard
  So that I can make timely and informed trading decisions

  Background: 
    Given I am logged in and I have "AAPL" in my watchlist

  Scenario: Viewing live updates for a stock in the watchlist
    When a new price for "AAPL" is received from the external data stream
    Then the dashboard should display the updated price for "AAPL"

  Scenario: Storing daily statistics
    When a new price for "AAPL" is received and it sets a new daily high
    Then the daily statistics for "AAPL" should be updated in the database
