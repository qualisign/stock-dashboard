// Using CommonJS syntax for Cucumber step definitions
import { Given, When, Then } from '@cucumber/cucumber';
import { expect } from 'chai';
import { render, screen, waitFor } from '@testing-library/react';
import sinon from 'sinon';
import ApplicationService from '../../src/application/applicationService';
import UserService from '../../src/domain/services/userService';
import WatchlistService from '../../src/domain/services/watchlistService';
import RealtimeService from '../../src/domain/services/realtimeService';
import DailyStatsService from '../../src/domain/services/dailyStatsService';
import React from 'react';
import { StockDashboard } from '../../src/interface/web/components/StockDashboard';


let userService, watchlistService, dailyStatsService, realtimeService, appService;
let mockWebSocketServer;

Given('I am logged in and I have "AAPL" in my watchlist', async function () {
  userService = new UserService();
  watchlistService = new WatchlistService();
  dailyStatsService = new DailyStatsService();
  
  // Inject the DailyStatsService into the RealtimeService
  realtimeService = new RealtimeService(dailyStatsService);
  
  appService = new ApplicationService(userService, watchlistService, realtimeService);
  
  sinon.stub(userService, 'login').resolves(true);
  sinon.stub(watchlistService, 'upsertWatchlist').resolves(['AAPL']);
  
  await appService.userService.login('user1', 'password');
  await appService.watchlistService.upsertWatchlist('user1', ['AAPL']);
  
  mockWebSocketServer = setupClientMockServer(8080); // Setup client-side mock WebSocket server
  render(React.createElement(StockDashboard, { websocketPort: 8080, apiEndpoint: "/api" }));
});

When('a new price for "AAPL" is received from the external data stream', function () {
  // Simulate receiving new stock price from the external data stream
  mockWebSocketServer.send(JSON.stringify({ symbol: 'AAPL', price: 150 }));
});

Then('the dashboard should display the updated price for "AAPL"', async function () {
  await waitFor(() => {
    expect(screen.getByText(`AAPL: $150`)).to.be.visible;
  });
});

When('a new price for "AAPL" is received and it sets a new daily high', function () {
  // Simulate receiving a new price that sets a new daily high
  const newHighPrice = 300;
  mockWebSocketServer.send(JSON.stringify({ symbol: 'AAPL', price: newHighPrice }));
});

Then('the daily statistics for "AAPL" should be updated in the database', function () {
  // Verify that the updateDailyHigh was called with the new high price
  sinon.assert.calledWith(dailyStatsService.updateDailyHigh, 'AAPL', 300);
});
