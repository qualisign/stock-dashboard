import WebSocket from 'ws';

export function setupPolygonMock(url) {
    const wss = new WebSocket.Server({ noServer: true });

    wss.on('connection', function connection(ws) {
        ws.on('message', function incoming(message) {
            console.log('received from client:', message);
            // Respond to client subscription requests or other interactions
            const data = JSON.parse(message);
            if (data.action === 'subscribe' && data.params.includes('AAPL')) {
                ws.send(JSON.stringify({ symbol: 'AAPL', price: 150 }));
            }
        });
    });

    // Simulate server is ready and can be connected to
    wss.on('listening', () => {
        console.log(`Polygon Mock Server running at ${url}`);
    });

    return wss;
}

export function setupClientMockServer(port) {
    const wss = new WebSocket.Server({ port });

    wss.on('connection', function connection(ws) {
        console.log('Client connected');

        ws.on('message', function incoming(message) {
            console.log('received from stock dashboard:', message);
            // This is where you would normally broadcast updates received from Polygon.io
            ws.send(JSON.stringify({ symbol: 'AAPL', price: 160 }));  // Sending a test message
        });

        ws.on('close', () => {
            console.log('Client disconnected');
        });
    });

    wss.on('listening', () => {
        console.log(`Client Mock Server running on port ${port}`);
    });

    return wss;
}
