Feature: User Management
  In order to access personalized features
  As a visitor
  I want to register and manage my user profile

  Scenario: User registration
    Given I am a new visitor
    When I provide my email, password, and confirm my password
    Then I should be registered as a new user

  Scenario: User login
    Given I am a registered user
    When I log in with my email and password
    Then I should gain access to my personalized dashboard

  Scenario: User logout
    Given I am logged in
    When I choose to log out
    Then I should be logged out and redirected to the homepage
