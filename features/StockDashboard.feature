Feature: Stock Dashboard
  In order to effectively monitor and manage stocks
  As a user
  I want to retrieve current stock prices and manage my watchlist

  Scenario: Retrieve current prices of all stocks
    Given the stock market data is available
    When I request the current stock prices
    Then I should receive the latest prices of all stocks

  Scenario: Add a new stock to the watchlist
    Given I am identified as a valid user
    When I add a new stock with symbol "AAPL" to my watchlist
    Then the stock with symbol "AAPL" should be added to my watchlist

  Scenario: View stocks in the watchlist
    Given I am identified as a valid user
    When I request to view my watchlist
    Then I should see all stocks currently in my watchlist
