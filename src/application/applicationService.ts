// src/application/applicationService.ts

import { IUserService, IRealtimeService, IAuthService } from '../domain/interfaces';

export interface IApplicationService {
  userService: IUserService;
  authService: IAuthService;
    realtimeService: IRealtimeService;


  loginUser(username: string, password: string): Promise<string>;
  registerUser(username: string, password: string, email: string): Promise<string>;
  logoutUser(userId: string): Promise<void>;
  startStreamingForUser(userId: string): Promise<void>;
    stopStreamingForUser(userId: string): Promise<void>;
}

export class ApplicationService {
  constructor(
    public userService: IUserService,
    public authService: IAuthService,
      public realtimeService: IRealtimeService,

  ) {}

  async loginUser(username: string, password: string): Promise<string> {
    if (!username || !password) {
      throw new Error("Username and password are required.");
    }
    const user = await this.userService.findByUsername(username);
    if (!user) {
      throw new Error("User not found.");
    }

    const isAuthenticated = await this.authService.validateUser(username, password);
    if (!isAuthenticated) {
      throw new Error("Authentication failed.");
    }

    return this.authService.generateToken(user);
  }

  async logoutUser(userId: string): Promise<void> {
    await this.userService.logout(userId);
    await this.realtimeService.stopStreamingForUser(userId);
  }

  async registerUser(username: string, password: string, email: string): Promise<string> {
    return this.userService.createUser(username, password, email);
  }

  async startStreamingForUser(userId: string): Promise<void> {
    console.log('from application service: started streaming for user with id: ' + userId)
    await this.realtimeService.startStreamingForUser(userId);
  }

  async stopStreamingForUser(userId: string): Promise<void> {
    await this.realtimeService.stopStreamingForUser(userId);
  }

  async upsertWatchlist(userId: string, watchlist: string[]): Promise<void> {
    // Your implementation here
}

async removeWatchlist(userId: string): Promise<void> {
  // Your implementation here
}

}
