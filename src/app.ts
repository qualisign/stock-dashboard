import express from 'express';
import http from 'http';
import { SqliteDailyStatsRepository } from './infrastructure/data/repositories/sqliteDailyStatsRepository';
import { SqliteUserRepository } from './infrastructure/data/repositories/sqliteUserRepository';
import { SqliteWatchlistRepository } from './infrastructure/data/repositories/sqliteWatchlistRepository';
import { InMemoryStockRepository } from './infrastructure/data/repositories/inMemoryStockRepository';
import { UserService } from './domain/services/userService';
import { AuthService } from './domain/services/authService';
import { WatchlistService } from './domain/services/watchlistService';
import { StockService } from './domain/services/stockService';
import { DailyStatsService } from './domain/services/dailyStatsService';
import { RealtimeService } from './domain/services/realtimeService';
import { ApplicationService } from './application/applicationService';
import { DuplexStreamAdapter } from "./infrastructure/communication/adapters/duplexStreamAdapter";
import { MockReadStreamAdapter } from "./infrastructure/communication/adapters/mockReadStreamAdapter";
import setupRoutes from './interface/api/routes.js';

import cors from 'cors';
import dotenv from 'dotenv';
dotenv.config();

const corsOptions = {
  origin: '*',  
  optionsSuccessStatus: 200,
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  allowedHeaders: "Content-Type,Authorization"
};

const createApp = async () => {
  const app = express();
  const httpServer = http.createServer(app);
  
  app.use(express.json());
  app.use(cors(corsOptions));

  // Middleware for logging requests
  app.use((req, res, next) => {
    console.log(`${req.method} ${req.originalUrl}`);
    next();
  });

  // Setting up database paths
  const databasePath = './src/infrastructure/data/stock_dashboard.db';

  // Repository initializations
  const userRepository = new SqliteUserRepository(databasePath);
  const watchlistRepository = new SqliteWatchlistRepository(databasePath);
  const dailyStatsRepository = new SqliteDailyStatsRepository(databasePath);
  const stockRepository = new InMemoryStockRepository();

  // Wait for the database setup before proceeding
  await dailyStatsRepository.setupDatabase();

  // Service initializations
  const authService = new AuthService(userRepository);
  const userService = new UserService(userRepository);
  const watchlistService = new WatchlistService(watchlistRepository);
  const stockService = new StockService(stockRepository);
  const dailyStatsService = new DailyStatsService(dailyStatsRepository);

  // Realtime service components
  const readStreamAdapter = new MockReadStreamAdapter();
  const wsAdapter = new DuplexStreamAdapter(readStreamAdapter, httpServer, authService);
  
  // RealtimeService might require an initialization or setup method
  const realtimeService = new RealtimeService(wsAdapter, dailyStatsService, watchlistService, stockService);

  // Application service setup
  const applicationService = new ApplicationService(userService, authService, realtimeService);


    setupRoutes(app, applicationService);

  return { app, httpServer, applicationService };
};

export { createApp };
