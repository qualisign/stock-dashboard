import { createApp } from './app.js';
import setupRoutes from './interface/api/routes.js';


async function startServer() {
    try {
        // Wait for the app to be created and configurations to be complete
        const { app, httpServer, applicationService } = await createApp();
        
        setupRoutes(app, applicationService);  

        const port = 3000;
        httpServer.listen(port, async () => {
            console.log(`Server running on port ${port}`);
            try {
                await applicationService.realtimeService.seedStocks();  // Seed stocks after server starts
                await applicationService.realtimeService.seedDailyStats(); // Seed daily stats
            } catch (error) {
                console.error('Error seeding data:', error);
            }
        });
    } catch (error) {
        console.error('Failed to initialize the application:', error);
    }
}

startServer();
