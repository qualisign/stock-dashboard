export interface TradeEvent {
  ev: string;  // Event type, e.g., 'T' for trade
  sym: string; // Symbol, e.g., 'MSFT'
  i: string;   // Trade ID
  x: number;   // Exchange ID
  p: number;   // Price of the trade
  s: number;   // Size of the trade
  t: number;   // Timestamp of the trade
  z: number;   // Tape ID
  c?: number[]; // Condition codes (optional)
}
