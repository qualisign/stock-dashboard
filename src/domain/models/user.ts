export type UserID = string; 

export type User = {
    id: UserID;
    username: string;
    passwordHash: string; 
    email: string;
};
