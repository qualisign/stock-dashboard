export const stockSymbols = [
    'AAPL',  // Apple
    'GOOGL', // Alphabet
    'AMZN',  // Amazon
    'MSFT',  // Microsoft
    'FB',    // Meta Platforms (formerly Facebook)
    'TSLA',  // Tesla
    'BRK.B', // Berkshire Hathaway
    'V',     // Visa
    'JNJ',   // Johnson & Johnson
    'WMT',   // Walmart
    'PG',    // Procter & Gamble
    'XOM',   // ExxonMobil
    'CVX',   // Chevron
    'BAC',   // Bank of America
    'JPM',   // JPMorgan Chase
    'RDS.A', // Royal Dutch Shell
    'INTC',  // Intel
    'CSCO',  // Cisco Systems
    'PFE',   // Pfizer
    'BA',    // Boeing
    'KO',    // Coca-Cola
    'ORCL',  // Oracle
    'NFLX',  // Netflix
    'CMCSA', // Comcast
    'PEP',   // PepsiCo
    'NKE',   // Nike
    'MCD',   // McDonald's
    'T',     // AT&T
    'ABBV',  // AbbVie
    'MMM'    // 3M
];

export type StockSymbol = typeof stockSymbols[number];

export interface Stock {
  symbol: StockSymbol;
  price: number;
}
