export interface Watchlist {
  watchlist: Array<string>;
  userId: string;
}
