export interface DailyStats {
    symbol: string;
    high: number;
    low: number;
    highDateTime: Date;
    lowDateTime: Date;
    closing: number;
}
