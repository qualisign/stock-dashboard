// src/domain/services/userService.ts

import { IUserService } from '../interfaces';
import { IUserRepository } from '../../infrastructure/data/repositories/IUserRepository';
import { User } from '../models/user';
import bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';



export class UserService implements IUserService {
  userRepository: IUserRepository;

  constructor(userRepository: IUserRepository) {
    this.userRepository = userRepository;
  }

  async createUser(username: string, password: string, email: string): Promise<string> {
    console.log(password)
    console.log(username)
    const passwordHash = await bcrypt.hash(password, 10);
    const userId = await this.userRepository.createUser({
      id: uuidv4(),
      username,
      passwordHash,
      email
    });
    console.log('the id of the created user: ' + userId);
    return userId
  }

  async deleteUser(userId: string): Promise<void> {
    await this.userRepository.deleteUser(userId);
  }

  async getUserId(username: string): Promise<string> {
    const user = await this.findByUsername(username);
    return user.id;
  }

  async findByUsername(username: string): Promise<User> {
    const user = await this.userRepository.findByUsername(username);
    if (!user) {
      throw new Error('User not found.');
    }
    return user;
  }

  async login(username: string, password: string): Promise<boolean> {
    const user = await this.findByUsername(username);
    if (!user) {
      return false;
    }
   
    return bcrypt.compare(password, user.passwordHash);
  }

  async logout(userId: string): Promise<void> {
    console.log(`User ${userId} logged out.`);
  }

  async isAuthenticated(userId: string): Promise<boolean> {
    const user = await this.userRepository.findById(userId);
    return !!user;
  }
}
