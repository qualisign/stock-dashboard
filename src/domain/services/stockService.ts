// src/domain/services/StockService.ts
import { IStockRepository } from '../../infrastructure/data/repositories/IStockRepository';
import { IStockService } from '../interfaces';
import { Stock } from '../models/stock';

export class StockService implements IStockService {
    constructor(private stockRepository: IStockRepository) {}

    public async upsertStock(symbol: string, price: number): Promise<void> {
        try {
            await this.stockRepository.upsertStock(symbol, price);
        } catch (error) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error(`Failed to upsert stock ${symbol} at price ${price}: ${message}`);
        }
    }

    public async deleteStock(symbol: string): Promise<void> {
        try {
            await this.stockRepository.deleteStock(symbol);
        } catch (error) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error(`Failed to delete stock ${symbol}: ${message}`);
        }
    }

    public async getStock(symbol: string): Promise<Stock | null> {
        try {
            return await this.stockRepository.getStock(symbol);
        } catch (error) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error(`Failed to retrieve stock ${symbol}: ${message}`);
        }
    }

    public async listStocks(): Promise<Stock[]> {
        try {
            return await this.stockRepository.listStocks();
        } catch (error) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error('Failed to list stocks: ' + message);
        }
    }
}
