import { IAuthService } from '../interfaces';
import { IUserRepository } from '../../infrastructure/data/repositories/IUserRepository';
import { User } from '../models/user';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { config } from "../../config";

const secret = config.jwtSecret;

export class AuthService implements IAuthService {
  constructor(private userRepository: IUserRepository) {}

  async validateUser(username: string, password: string): Promise<string | null> {
    const user = await this.userRepository.findByUsername(username);
    if (user && await bcrypt.compare(password, user.passwordHash)) {
      return this.generateToken(user);
    }
    return null;
  }

  async verifyToken(token: string): Promise<User | null> {
try {
    console.log('Verifying token with secret:', secret);
    console.log('verifying token:')
    console.log(token)
  const decoded = jwt.verify(token, secret);
  console.log('Decoded token:', decoded);
  return decoded as User;
} catch (error) {
  console.log('Error verifying token:', error);
  return null;
}

  }

  async generateToken(user: User): Promise<string> {
    const payload = { email: user.email, id: user.id, username: user.username, iat: Math.floor(Date.now() / 1000) };
      console.log('Signing token:', payload);
      const token = jwt.sign(payload, secret, { expiresIn: '1h' });
console.log('Generated JWT:', token);
return token;
      return token;
  }
}
