import { IDailyStatsRepository } from "../../infrastructure/data/repositories/IDailyStatsRepository";
import { DailyStats } from "../models/dailyStats";
import { Stock } from "../models/stock";
import { TradeEvent } from "../models/tradeEvent";
import { IDailyStatsService } from "../interfaces"

export class DailyStatsService implements IDailyStatsService {
  private statsCache: Map<string, DailyStats>;
  private dailyStatsRepository: IDailyStatsRepository;
  

  constructor(dailyStatsRepository: IDailyStatsRepository) {
    this.statsCache = new Map<string, DailyStats>();
    this.dailyStatsRepository = dailyStatsRepository;

  }

  public async handleStockUpdates(events: TradeEvent[]): Promise<void> {
        for (const event of events) {
            const existingStats = this.statsCache.get(event.sym) || this.createInitialStats(event);
            const updated = this.updateStatsFromEvent(existingStats, event);
            if (updated) {
                this.statsCache.set(event.sym, existingStats);
                await this.updateDatabaseWithStats(existingStats);
            }
        }
    }
private createInitialStats(event: TradeEvent): DailyStats {
    return {
        symbol: event.sym,
        high: event.p,
        low: event.p,
        closing: event.p,
        highDateTime: new Date(), 
        lowDateTime: new Date()   
    };
}
  

    private updateStatsFromEvent(stats: DailyStats, event: TradeEvent): boolean {
        let updated = false;
        if (event.p > stats.high) {
            stats.high = event.p;
            updated = true;
        }
        if (event.p < stats.low) {
            stats.low = event.p;
            updated = true;
        }
        // Assuming the latest trade gives the closing price
        stats.closing = event.p;
        updated = true;
        return updated;
    }
  async updateDatabaseWithStats(stats: DailyStats): Promise<void> {
    console.log(`Updating database for symbol: ${stats.symbol}`, stats);
    await this.dailyStatsRepository.upsertDailyStats(stats);
  }

    public async listDailyStats(): Promise<DailyStats[]> {
        return this.dailyStatsRepository.listDailyStats() || []
    }    

    public async getDailyStats(stockSymbol: string): Promise<DailyStats | null> {
        return this.statsCache.get(stockSymbol) || null;
    }
}

