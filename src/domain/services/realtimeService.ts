import { IWatchlistService, IDailyStatsService, IRealtimeService, IDuplexStreamAdapter, IStockService } from "../interfaces";
import { TradeEvent } from "../models/tradeEvent";
import { StockSymbol } from "../models/stock";
import { UserID } from "../models/user";
import WebSocket from 'ws';
import { createStocks } from "../../infrastructure/data/seeding/stockFactory";
import { createDailyStats } from "../../infrastructure/data/seeding/dailyStatsFactory";

export class RealtimeService implements IRealtimeService {
    private websocketAdapter: IDuplexStreamAdapter;
    dailyStatsService: IDailyStatsService;
    watchlistService: IWatchlistService;
    stockService: IStockService;
    private subscribersBySymbol: Map<StockSymbol, Set<UserID>>;
    private socketsBySubscriber: Map<UserID, WebSocket>;

    constructor(
        websocketAdapter: IDuplexStreamAdapter,
        dailyStatsService: IDailyStatsService,
        watchlistService: IWatchlistService,
        stockService: IStockService    ) {
        this.websocketAdapter = websocketAdapter;
        this.dailyStatsService = dailyStatsService;
        this.watchlistService = watchlistService;
        this.stockService = stockService;
        this.subscribersBySymbol = new Map<StockSymbol, Set<UserID>>();
        this.socketsBySubscriber = new Map<UserID, WebSocket>();
        this.initializeListeners();
    }

    private initializeListeners(): void {
        this.websocketAdapter.on('clientConnected', this.handleClientConnected.bind(this));
        this.websocketAdapter.on('clientDisconnected', this.handleClientDisconnected.bind(this));
        this.websocketAdapter.on('tradeEvent', this.handleTradeEvents.bind(this));
	this.websocketAdapter.on('subscribe', this.subscribeUserToSymbol.bind(this));
	this.websocketAdapter.on('unsubscribe', this.unsubscribeUserFromSymbol.bind(this));	
    }

    private handleClientConnected({ socket, userId }): void {
        console.log(`Client connected with ID: ${userId}, storing WebSocket with id ${socket.id}`);
        if (!socket) {
            console.log("Error: Socket is undefined at connection.");
            return;
        }
        this.socketsBySubscriber.set(userId, socket);
        this.setupClientSubscriptions(userId).then(() => {
            console.log(`Subscriptions setup for user ${userId}`);
        }).catch(error => {
            console.error(`Failed to setup subscriptions for user ${userId}: ${error}`);
        });
    }

    private handleClientDisconnected({ userId }): void {
        console.log(`Client disconnected with ID: ${userId}`);
        this.socketsBySubscriber.delete(userId);
        // Remove this user from all subscriptions
        this.subscribersBySymbol.forEach((users, symbol) => {
            users.delete(userId);
            if (users.size === 0) {
                this.subscribersBySymbol.delete(symbol);
                console.log(`No more subscribers for symbol: ${symbol}, removing from map`);
            }
        });
    }

    private async handleTradeEvents(tradeEvents: TradeEvent[]): Promise<void> {
        console.log('Handling trade event in RealtimeService');
        if (Array.isArray(tradeEvents)) {
            for (const event of tradeEvents) {
                console.log(`Upserting stock: ${event.sym} at price ${event.p}`);
                await this.stockService.upsertStock(event.sym, event.p);
                this.broadcastTradeEvent(event);
            }
        }
    }

async broadcastTradeEvent(event: TradeEvent): Promise<void> {
    console.log(`Broadcasting trade event ${event.sym} to subscribers`);
    const subscribers = this.subscribersBySymbol.get(event.sym);
    if (!subscribers || subscribers.size === 0) {
        console.log(`No subscribers for symbol ${event.sym}`);
        return;
    }
    console.log(`Subscribers for ${event.sym}: ${Array.from(subscribers)}`);
    subscribers.forEach(userId => {
        const socket = this.socketsBySubscriber.get(userId);
        if (socket) {
            console.log(`Sending event to user ${userId}`);
            socket.emit('tradeEvent', event);  // Make sure to send a serialized object
        } else {
            console.log(`Socket not ready or not available for user ${userId}`);
        }
    });
}

    // Example methods in RealtimeService
subscribeUserToSymbol(userId: string, symbol: StockSymbol): void {
    if (!this.subscribersBySymbol.has(symbol)) {
        this.subscribersBySymbol.set(symbol, new Set());
    }
    this.subscribersBySymbol.get(symbol).add(userId);
    console.log(`User ${userId} subscribed to ${symbol}`);
}

unsubscribeUserFromSymbol(userId: string, symbol: StockSymbol): void {
    const subscribers = this.subscribersBySymbol.get(symbol);
    if (subscribers && subscribers.delete(userId)) {
        console.log(`User ${userId} unsubscribed from ${symbol}`);
        if (subscribers.size === 0) {
            this.subscribersBySymbol.delete(symbol);
        }
    }
}

    

async setupClientSubscriptions(userId: string): Promise<void> {
    const symbols = await this.watchlistService.getWatchlist(userId);
    console.log(`Setting up subscriptions for user ${userId} with symbols: ${symbols}`);
    symbols.forEach(symbol => {
        if (!this.subscribersBySymbol.has(symbol)) {
            this.subscribersBySymbol.set(symbol, new Set());
        }
        this.subscribersBySymbol.get(symbol).add(userId);
        console.log(`Added ${userId} to subscribers for ${symbol}`);
    });
}

async stopStreamingForUser(userId: string): Promise<void> {
    this.subscribersBySymbol.forEach((users, symbol) => {
        if (users.delete(userId)) {
            console.log(`Removed ${userId} from subscribers for ${symbol}`);
        }
        if (users.size === 0) {
            this.subscribersBySymbol.delete(symbol);
            console.log(`No more subscribers for symbol: ${symbol}, removing from map`);
        }
    });
    console.log(`Stopped streaming for user ${userId}`);
}
    
    

    public async startStreamingForUser(userId: string): Promise<void> {
        await this.setupClientSubscriptions(userId);
    }

    public stopService(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.websocketAdapter.stopService();
                console.log("Service stopped.");
                resolve();
            } catch (error) {
                console.error(`Error stopping service: ${error}`);
                reject(error);
            }
        });
    }

    async seedStocks(): Promise<void> {
	// Assuming createStocks is a function that generates stock data
	const stocks = createStocks();
	for (const stock of stocks) {
	    await this.stockService.upsertStock(stock.symbol, stock.price);
	}
	console.log('Stocks seeded successfully.');
    }

    
    async seedDailyStats(): Promise<void> {
	// Assuming createStocks is a function that generates stock data
	const dailyStats = createDailyStats();
	for (const stats of dailyStats) {
	    await this.dailyStatsService.updateDatabaseWithStats(stats);
	}
	console.log('Daily stats seeded successfully.');
    }


    
    public unsubscribeAll(userId: string): Promise<void> {
        return new Promise((resolve) => {
            console.log(`Unsubscribing all for ${userId}`);
            this.stopStreamingForUser(userId);
            resolve();
        });
    }
}
