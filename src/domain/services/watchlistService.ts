import { IWatchlistService } from "../interfaces";
import { IWatchlistRepository } from '../../infrastructure/data/repositories/IWatchlistRepository';

export class WatchlistService implements IWatchlistService {
    private watchlistRepository: IWatchlistRepository;

    constructor(watchlistRepository: IWatchlistRepository) {
        this.watchlistRepository = watchlistRepository;
    }

    async getWatchlist(userId: string): Promise<string[]> {
        try {
            return await this.watchlistRepository.getWatchlistByUserId(userId);
        } catch (error: unknown) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error(`Failed to retrieve watchlist for user ID ${userId}: ${message}`);
        }
    }

async updateWatchlist(userId: string, stocksToAdd: string[] = [], stockToRemove?: string): Promise<void> {
    // Retrieve current watchlist
    const currentWatchlist = await this.watchlistRepository.getWatchlistByUserId(userId);
    let updatedWatchlist = [...currentWatchlist];

    // Add new stocks
    updatedWatchlist = updatedWatchlist.concat(stocksToAdd.filter(stock => !currentWatchlist.includes(stock)));

    // Remove a stock if specified
    if (stockToRemove) {
        updatedWatchlist = updatedWatchlist.filter(stock => stock !== stockToRemove);
    }

    // Update the watchlist in the database
    await this.watchlistRepository.upsertWatchlist(userId, updatedWatchlist);
}
    

    async upsertWatchlist(userId: string, symbols: string[]): Promise<void> {
        try {
            await this.watchlistRepository.upsertWatchlist(userId, symbols);
        } catch (error: unknown) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error(`Failed to upsert watchlist for user ID ${userId}: ${message}`);
        }
    }

    async deleteWatchlist(userId: string): Promise<void> {
        try {
            await this.watchlistRepository.deleteWatchlistByUserId(userId);
        } catch (error: unknown) {
            const message = error instanceof Error ? error.message : 'Unknown error';
            throw new Error(`Failed to delete watchlist for user ID ${userId}: ${message}`);
        }
    }
}
