import { DailyStats } from "./models/dailyStats";
import { Stock, StockSymbol } from "./models/stock";
import { User, UserID } from "./models/user";
import { TradeEvent } from "./models/tradeEvent";
import { EventEmitter } from 'events';
import { IUserRepository } from "../infrastructure/data/repositories/IUserRepository";
import http from 'http';

export interface IAuthService {
    validateUser(username: string, password: string): Promise<string | null>;
  verifyToken(token: string): Promise<User | null>;
  generateToken(user: User): Promise<string>;
}

export interface IStockService {
    /**
     * Upserts a stock in the database. If the stock already exists, its price is updated;
     * otherwise, a new stock entry is created.
     * @param symbol The stock symbol, such as 'AAPL', 'GOOGL', etc.
     * @param price The current price of the stock.
     * @throws Throws an error if the operation cannot be completed.
     */
    upsertStock(symbol: string, price: number): Promise<void>;

    /**
     * Deletes a stock entry based on its symbol.
     * @param symbol The stock symbol to delete.
     * @throws Throws an error if the deletion cannot be completed.
     */
    deleteStock(symbol: string): Promise<void>;

    /**
     * Retrieves a stock entry based on its symbol.
     * @param symbol The stock symbol to retrieve.
     * @returns The stock object if found, otherwise null.
     */
    getStock(symbol: string): Promise<Stock | null>;

    /**
     * Lists all stocks in the database.
     * @returns An array of all stock objects.
     */
    listStocks(): Promise<Stock[]>;
}



export interface IUserService {
  userRepository: IUserRepository;
  login(username: string, password: string): Promise<boolean>;
    logout(userId: string): Promise<void>;
    createUser(username: string, password: string, email: string): Promise<string>;
    deleteUser(userId: string): Promise<void>;
    isAuthenticated(userId: string): Promise<boolean>;
    getUserId(username: string): Promise<string>;
  findByUsername(username: string): Promise<User>;
}


export interface IWatchlistService {
  getWatchlist(userId: string): Promise<string[]>; // Retrieve all stock symbols in a user's watchlist
  upsertWatchlist(userId: string, symbols: string[]): Promise<void>; // Create or update a watchlist
  deleteWatchlist(userId: string): Promise<void>; // Delete a user's watchlist
}


export interface IDailyStatsService {
    getDailyStats(stockSymbol: string): Promise<DailyStats | null>;
  listDailyStats(): Promise<DailyStats[]>;    
    handleStockUpdates(events: TradeEvent[]): Promise<void>;
    updateDatabaseWithStats(stats: DailyStats): Promise<void>;
    
}

export interface IRealtimeService {
  dailyStatsService: IDailyStatsService;
    watchlistService: IWatchlistService;
    stockService: IStockService;
  broadcastTradeEvent(data: any): Promise<void>; // Pushes data received from the data provider to connected clients
  unsubscribeAll(userId: string): Promise<void>; // Unsubscribes from all stock updates for a user
  stopService(): Promise<void>; // Stops the WebSocket server and any ongoing data streams
  startStreamingForUser(userId: string): Promise<void>;
    stopStreamingForUser(userId: string): Promise<void>;
    seedStocks(): Promise<void>;
    seedDailyStats(): Promise<void>;
    subscribeUserToSymbol(userId: UserID, symbol: StockSymbol): void;
    unsubscribeUserFromSymbol(userId: UserID, symbol: StockSymbol): void;
						      
}

export interface IDuplexStreamAdapter extends EventEmitter  {
  // Initializes the WebSocket server and sets up the necessary event listeners
    // Sets up the connection to the external data stream (e.g., Polygon.io)
  initializeWebSocketServer(httpServer: http.Server, port: number): void;

  // Broadcasts data to all subscribed clients
  stopService(): void;
}

export interface IReadStreamAdapter extends EventEmitter {
    initializeDataStream(): void;  // Set up and start the data stream
    closeDataStream(): void;       // Clean up and close the data stream
}
