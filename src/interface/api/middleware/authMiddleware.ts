import { Request, Response, NextFunction } from 'express';
import { IAuthService } from '../../../domain/interfaces';
import { User } from '../../../domain/models/user';
import { config } from "../../../config";

export interface AuthRequest extends Request {
  user?: User;
}
const secret = config.jwtSecret;
console.log('inside of auth');
console.log({secret});
export function authMiddleware(authService: IAuthService) {
  return async (req: AuthRequest, res: Response, next: NextFunction) => {
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
      return res.status(401).send({ message: 'Authentication token required.' });
    }
    try {
      //const decoded = jwt.verify(token, secret); // Use environment variable for security
	const user = await authService.verifyToken(token); // Assuming token contains user ID
      if (!user) {
        return res.status(401).send({ message: 'Token invalid or expired.' });
      }
      req.user = user;
      next();
    } catch (error) {
      console.error('Authentication error:', error);
      return res.status(500).send({ message: 'Error processing authentication.' });
    }
  };
}

