// src/interface/api/routes.js
import express, { Express } from 'express';
import { UserController } from './controllers/userController';
import { WatchlistController } from './controllers/watchlistController';
import { DailyStatsController } from './controllers/dailyStatsController';
import { TradeEventsController } from './controllers/tradeEventsController';
import { StockController } from './controllers/stockController';  
import { IApplicationService } from '../../application/applicationService';
import { authMiddleware } from './middleware/authMiddleware';

export default function setupRoutes(app: Express, appService: IApplicationService) {
  const { authService, realtimeService, userService } = appService;
  
  const userController = new UserController(userService, authService);
  const watchlistController = new WatchlistController(realtimeService.watchlistService);
  const dailyStatsController = new DailyStatsController(realtimeService.dailyStatsService);
  const tradeEventsController = new TradeEventsController(realtimeService);
  const stocksController = new StockController(realtimeService.stockService); 

  // Public routes
  app.use('/api/users', userController.router);

  // Protected routes
  app.use('/api/watchlist', authMiddleware(authService), watchlistController.router);
  app.use('/api/dailystats', authMiddleware(authService), dailyStatsController.router);
  app.use('/api/tradeevents', authMiddleware(authService), tradeEventsController.router);
  app.use('/api/stocks', authMiddleware(authService), stocksController.router);  
}
