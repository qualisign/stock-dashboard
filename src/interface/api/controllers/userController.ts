// src/interface/api/controllers/userController.js
import { Router, Request, Response } from 'express';
import { IUserService, IAuthService } from "../../../domain/interfaces";

export class UserController {
    public router: Router;

    constructor(private userService: IUserService, private authService: IAuthService) {
        this.router = Router();
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        this.router.post('/login', this.login.bind(this));
        this.router.post('/register', this.register.bind(this));
        this.router.delete('/:userId', this.deleteUser.bind(this));
    }

  private async login(req: Request, res: Response): Promise<void> {
    try {
        const { username, password } = req.body;
        const user = await this.userService.findByUsername(username);
        if (!user) {
            res.status(401).json({ success: false, message: "User not found" });
            return;
        }
        const token = await this.authService.validateUser(username, password);
        if (token) {
            res.json({ success: true, token: token });
        } else {
            res.status(401).json({ success: false, message: "Authentication failed" });
        }
    } catch (error) {
        console.error("Error during login:", error);
        res.status(500).json({ success: false, message: "Internal server error" });
    }
  }



  private async register(req: Request, res: Response): Promise<void> {
    try {
      console.log(req)
      
      const { username, password, email } = req.body;
      console.log(req.body)

      const userId = await this.userService.createUser(username, password, email);
      res.status(201).json({ userId });
        } catch (error) {
            const message = (error as Error).message;
            res.status(500).json({ message });
        }
    }

    private async deleteUser(req: Request, res: Response): Promise<void> {
        try {
            await this.userService.deleteUser(req.params.userId);
            res.status(204).send();
        } catch (error) {
            const message = (error as Error).message;
            res.status(500).json({ message });
        }
    }
};
