import { IWatchlistService } from "../../../domain/interfaces";
import { Router, Request, Response } from 'express';

export class WatchlistController {
    public router: Router;

    constructor(private watchlistService: IWatchlistService) {
        this.router = Router();
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        this.router.get('/:userId', this.getWatchlist.bind(this));
        this.router.put('/:userId/:stock', this.subscribe.bind(this));
        this.router.delete('/:userId/:stock', this.unsubscribe.bind(this));
        this.router.delete('/:userId', this.deleteWatchlist.bind(this));
    }

    private async subscribe(req: Request, res: Response): Promise<void> {
        try {
            const userId = req.params.userId;
            const stock = req.params.stock;
            const watchlist = await this.watchlistService.getWatchlist(userId);
            if (!watchlist.includes(stock)) {
                await this.watchlistService.upsertWatchlist(userId, [...watchlist, stock]);
                res.status(200).json({ message: `Subscribed to ${stock}` });
            } else {
                res.status(400).json({ message: `Already subscribed to ${stock}` });
            }
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).json({ error: message });
        }
    }

    private async unsubscribe(req: Request, res: Response): Promise<void> {
        try {
            const userId = req.params.userId;
            const stock = req.params.stock;
            const watchlist = await this.watchlistService.getWatchlist(userId);
            if (watchlist.includes(stock)) {
                const updatedWatchlist = watchlist.filter(s => s !== stock);
                await this.watchlistService.upsertWatchlist(userId, updatedWatchlist);
                res.status(200).json({ message: `Unsubscribed from ${stock}` });
            } else {
                res.status(400).json({ message: `Not subscribed to ${stock}` });
            }
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).json({ error: message });
        }
    }

    private async getWatchlist(req: Request, res: Response): Promise<void> {
        try {
            const userId = req.params.userId;
            const watchlist = await this.watchlistService.getWatchlist(userId);
            res.json(watchlist);
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).send(message);
        }
    }

    private async deleteWatchlist(req: Request, res: Response): Promise<void> {
        try {
            const userId = req.params.userId;
            await this.watchlistService.deleteWatchlist(userId);
            res.status(204).send();
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).send(message);
        }
    }
}
