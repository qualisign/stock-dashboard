import { Router, Request, Response } from 'express';
import { IDailyStatsService } from "../../../domain/interfaces";

export class DailyStatsController {
    public router: Router;

    constructor(private dailyStatsService: IDailyStatsService) {
        this.router = Router();
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        // Define the route to handle requests for daily statistics with a symbol
        this.router.get('/:symbol', this.getDailyStats.bind(this)); 
        this.router.get('/', this.listDailyStats.bind(this)); 
    }

    private async listDailyStats(req: Request, res: Response): Promise<void> {
    console.log('listing daily stats');
    try {
        const stats = await this.dailyStatsService.listDailyStats();
        if (stats) {
            res.json(stats);
        } else {
            // If no stats are available, you might want to send a different response, e.g.:
            res.status(404).json({ error: 'No stats available' });
        }
    } catch (error) {
        console.error("Failed to retrieve daily stats:", error);
        res.status(500).json({ error: 'Failed to retrieve daily stats' });
    }
}


    
    private async getDailyStats(req: Request, res: Response): Promise<void> {
      try {
        const symbol = req.params.symbol;

        // Check if the symbol parameter is provided in the request
        if (!symbol) {
          // Send a 400 Bad Request response if the symbol is missing
          res.status(400).json({ error: 'Stock symbol is required' });
          return;
        }

        // Retrieve the daily stats for the specified symbol
        const stats = await this.dailyStatsService.getDailyStats(symbol);
        if (stats) {
          res.json(stats);
        } else {
          // Respond with a 404 Not Found if no stats are available for the symbol
          res.status(404).json({ error: `No daily stats available for symbol: ${symbol}` });
        }
      } catch (error) {
        // Log the error for debugging purposes
        console.error("Failed to retrieve daily stats:", error);
        // Send a 500 Internal Server Error response if an exception occurs
        res.status(500).json({ error: 'Failed to retrieve daily stats' });
      }
    }
}
