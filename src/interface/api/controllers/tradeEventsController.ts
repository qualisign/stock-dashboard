import { Router, Request, Response } from 'express';
import { IRealtimeService } from '../../../domain/interfaces';
import { AuthRequest } from '../middleware/authMiddleware';


export class TradeEventsController {
  public router: Router;
  private realtimeService: IRealtimeService;

  constructor(realtimeService: IRealtimeService) {
    this.realtimeService = realtimeService;
    this.router = Router();
    this.initializeRoutes();
  }

  private initializeRoutes(): void {
    this.router.get('/start', this.startStream.bind(this));
    this.router.get('/stop', this.stopStream.bind(this));
  }

 
  // Assuming the methods are part of a class like TradeEventsController
  private async startStream(req: AuthRequest, res: Response): Promise<Response> {
    console.log('User ID from request:', req.user ? req.user.id : 'No user ID found');
    if (!req.user) {
      return res.status(401).send('Authentication required');
  }


  const userId = req.user.id;

  try {
    await this.realtimeService.startStreamingForUser(userId);
    return res.json({ success: true, message: 'Started stream for user ' + userId });
  } catch (error) {
    return res.status(500).json({ success: false, message: 'Failed to subscribe to events.' });
  }
}

private async stopStream(req: AuthRequest, res: Response): Promise<Response> {
  if (!req.user || !req.user) {
    return res.status(401).send('Authentication required');
  }
  const userId = req.user.id;

  try {
    await this.realtimeService.stopStreamingForUser(userId);
    return res.json({ success: true, message: 'Stopped stream for user ' + userId });
  } catch (error) {
    return res.status(500).json({ success: false, message: 'Failed to unsubscribe from events.' });
  }
}

}
