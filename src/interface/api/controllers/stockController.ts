// src/interface/api/controllers/stockController.js
import { Router, Request, Response } from 'express';
import { IStockService } from "../../../domain/interfaces";
import { Stock } from "../../../domain/models/stock";

export class StockController {
    public router: Router;

    constructor(private stockService: IStockService) {
        this.router = Router();
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        this.router.put('/:symbol', this.upsertStock.bind(this));
        this.router.delete('/:symbol', this.deleteStock.bind(this));
        this.router.get('/:symbol', this.getStock.bind(this));
        this.router.get('/', this.listStocks.bind(this));
    }

    private async upsertStock(req: Request, res: Response): Promise<void> {
        try {
            const { symbol } = req.params;
            const { price } = req.body;
            await this.stockService.upsertStock(symbol, parseFloat(price));
            res.json({ success: true, message: 'Stock updated successfully', symbol, price });
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).json({ success: false, message });
        }
    }

    private async deleteStock(req: Request, res: Response): Promise<void> {
        try {
            const { symbol } = req.params;
            await this.stockService.deleteStock(symbol);
            res.status(204).send();
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).json({ success: false, message });
        }
    }

    private async getStock(req: Request, res: Response): Promise<void> {
        try {
            const { symbol } = req.params;
            const stock = await this.stockService.getStock(symbol);
            if (stock) {
                res.json(stock);
            } else {
                res.status(404).json({ success: false, message: 'Stock not found' });
            }
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).json({ success: false, message });
        }
    }

    private async listStocks(req: Request, res: Response): Promise<void> {
        try {
	    console.log('in stock controller before')
            const stocks = await this.stockService.listStocks();
	    console.log('in stock controller after')
            res.json(stocks);
        } catch (error) {
            const message = (error instanceof Error) ? error.message : 'An unknown error occurred';
            res.status(500).json({ success: false, message });
        }
    }
}
