import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { CircularProgress, Alert, Box, Typography, Paper } from '@mui/material';
import StockCard from "./StockCard";
import { useAuth } from '../contexts/AuthContext';
import { useWebSocket } from '../contexts/WebSocketContext';

export const Stocks = () => {
    const [watchlist, setWatchlist] = useState<string[]>([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);
    const { user, token } = useAuth();
    const { stocks, sendSubscriptionUpdate } = useWebSocket();

    useEffect(() => {
        const fetchWatchlist = async () => {
            try {
                const response = await axios.get<string[]>(`http://localhost:3000/api/watchlist/${user?.id}`, {
                    headers: { Authorization: `Bearer ${token}` }
                });
                setWatchlist(response.data);
            } catch (error) {
                setError('Failed to fetch watchlist');
            } finally {
                setLoading(false);
            }
        };

        if (user?.id && token) {
            fetchWatchlist();
        }
    }, [user?.id, token]);

    const toggleSubscription = async (symbol: string) => {
        setLoading(true);
        const isSubscribed = watchlist.includes(symbol);
        try {
            if (isSubscribed) {
                await axios.delete(`http://localhost:3000/api/watchlist/${user?.id}/${symbol}`, {
                    headers: { Authorization: `Bearer ${token}` }
                });
                setWatchlist(prev => prev.filter(item => item !== symbol));
                sendSubscriptionUpdate(symbol, 'unsubscribe');
            } else {
                await axios.put(`http://localhost:3000/api/watchlist/${user?.id}/${symbol}`, {}, {
                    headers: { Authorization: `Bearer ${token}` }
                });
                setWatchlist(prev => [...prev, symbol]);
                sendSubscriptionUpdate(symbol, 'subscribe');
            }
        } catch (err) {
            setError(`Failed to ${isSubscribed ? 'unsubscribe from' : 'subscribe to'} ${symbol}`);
        } finally {
            setLoading(false);
        }
    };

    return (
      <div>
        <Box sx={{ position: 'relative', minHeight: 40 }}>
          {loading && (
            <CircularProgress sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%,-50%)' }} />
	  )}
	</Box>
	{error && <Alert severity="error" sx={{ marginBottom: 2 }}>{error}</Alert>}
	<Box sx={{ marginTop: -3, display: 'flex', flexWrap: 'wrap', gap: 1, justifyContent: 'space-around' }}>
	  {stocks.map(stock => (
	    <StockCard
	      key={stock.symbol}
	      symbol={stock.symbol}
	      price={stock.price}
	      subscribed={watchlist.includes(stock.symbol)}
	      onToggleSubscription={() => toggleSubscription(stock.symbol)}
	    />
	  ))}
	</Box>
      </div>
    );
};

export default Stocks;
