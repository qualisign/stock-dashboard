import React, { useState } from 'react';
import { Tab, Tabs, Box, Paper } from '@mui/material';
import LiveTrades from './LiveTrades'; // Updated import if necessary
import { Login } from './Login';
import { Register } from './Register';
import Stocks from './Stocks';
import DailyStats from './DailyStats';

const StockDashboard = () => {
    const [value, setValue] = useState(3); // Initially open the Register tab
    const pages = [<Stocks />, <LiveTrades />, <DailyStats />, <Login />, <Register />];
    const labels = ['Stocks', 'Live Trades', 'Historical Data', 'Login', 'Register'];

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    return (

      <div>
        <Tabs value={value} onChange={handleChange} aria-label="navigation tabs" variant="fullWidth" textColor="primary" indicatorColor="primary"  sx={{
          borderBottom: 1, 
          borderColor: 'rgba(117, 124, 232, 0.3)', 
          position: 'sticky', 
          top: 0, 
          zIndex: 1100,
          boxShadow: '0px 1px 1px -1px rgba(0,0,0,0.2)', 
          backgroundColor: 'white'
        }}>
          {labels.map((label, index) => (
            <Tab label={label} key={index} sx={{ fontWeight: 'bold', fontSize: '1rem' }} />
          ))}
        </Tabs>
        <div style={{backgroundImage: 'linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%)'}}>
          {pages[value]}
        </div>
      </div>

    );
};

export default StockDashboard;
