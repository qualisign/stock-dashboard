import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useAuth } from '../contexts/AuthContext';

const port = process.env.REACT_APP_WS_PORT;

// Props should include userId to fetch the specific watchlist
export const Watchlist = ({ }) => {
  const { user, token } = useAuth();
  console.log(user)
  const userId = user?.id;
  const config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  
  const [watchlist, setWatchlist] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  // Fetch watchlist data from the server
  useEffect(() => {
    const fetchWatchlist = async () => {
      setLoading(true);
      try {

	console.log({token})
        const response = await axios.get(`http://localhost:${port}/api/watchlist/${userId}`, config);
	console.log(response)
        setWatchlist(response.data); 
      } catch (err) {
	
        setError('Failed to fetch watchlist');
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    fetchWatchlist();
  }, [userId]);

  // Function to update the watchlist
  const updateWatchlist = async (stocks:any) => {
    try {
      await axios.put(`http://localhost:${port}/api/watchlist/${userId}`, { stocks }, config);
      setWatchlist(stocks);
    } catch (err) {
      setError('Failed to update watchlist');
      console.error(err);
    }
  };

  // Function to delete the watchlist
  const deleteWatchlist = async () => {
    try {
      await axios.delete(`http://localhost:${port}/api/watchlist/${userId}`);
      setWatchlist([]); // Clear the local state
    } catch (err) {
      setError('Failed to delete watchlist');
      console.error(err);
    }
  };

  return (
    <div>
      <h2>My Watchlist</h2>
      {loading ? <p>Loading...</p> : (
        <ul>
          {watchlist.map((stock, index) => (
            <li key={index}>{stock}</li>
          ))}
        </ul>
      )}
      {error && <p>{error}</p>}
    </div>
  );
};
