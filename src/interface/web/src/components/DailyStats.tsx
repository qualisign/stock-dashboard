import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { DailyStats as DailyStatsType } from '../../../../domain/models/dailyStats';
import { useAuth } from '../contexts/AuthContext';
import { CircularProgress, Typography, Card, CardContent, TextField, Grid, Box } from '@mui/material';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import DatePicker from '@mui/lab/DatePicker';

export const DailyStats = () => {
  const [stats, setStats] = useState<DailyStatsType[]>([]);
  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date());
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const { token } = useAuth();

  useEffect(() => {
    const fetchDailyStats = async () => {
      const dateString = selectedDate ? selectedDate.toISOString().slice(0, 10) : '';
      try {
        const response = await axios.get(`http://localhost:3000/api/dailystats?date=${dateString}`, {
          headers: { Authorization: `Bearer ${token}` }
        });
        console.log(response.data);
        setStats(response.data);
      } catch (error) {
        setError('Failed to fetch daily stats');
      } finally {
        setLoading(false);
      }
    };

    if (selectedDate) {
      fetchDailyStats();
    }
  }, [selectedDate, token]);

  const handleDateChange = (date: Date | null) => {
    setLoading(true);
    setSelectedDate(date);
  };

  return (
    <div style={{ padding: '2em' }}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
          <Typography variant="h5">Daily Stats</Typography>
          <DatePicker
            label="Select Date"
            value={selectedDate}
            onChange={handleDateChange}
            renderInput={(params:any) => <TextField {...params} />}
          />
        </Box>
      </LocalizationProvider>

      {loading ? <CircularProgress /> : (
        <Grid container spacing={2}>
          {stats.map((stat, index) => (
            <Grid item xs={1.5} key={index}>
              <Card variant="outlined">
                <CardContent>
                  <Box display="flex" justifyContent="space-between" alignItems="center">
                    <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                      {stat.symbol}
                    </Typography>
                  </Box>
                  <Typography variant="body2" color="textPrimary">
                    {new Date(stat.highDateTime).toLocaleDateString()}
                  </Typography>		  
                  <Typography variant="body1">
                    High: ${stat.high}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    at {new Date(stat.highDateTime).toLocaleTimeString()}
                  </Typography>
                  <Typography variant="body1">
                    Low: ${stat.low}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    at {new Date(stat.lowDateTime).toLocaleTimeString()}
                  </Typography>
                  <Typography variant="body1">
                    Closing: ${stat.closing}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      )}

      {error && <Typography color="error">{error}</Typography>}
    </div>
  );
};

export default DailyStats;
