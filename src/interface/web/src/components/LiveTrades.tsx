import React from 'react';
import { useWebSocket } from '../contexts/WebSocketContext';
import { TradeEvent } from "../../../../domain/models/tradeEvent";
import { List, ListItem, Typography, Paper, Card, CardContent, Grid, Chip } from '@mui/material';
import Fade from '@mui/material/Fade';


export const LiveTrades = () => {
  const { tradeEvents } = useWebSocket();

    const getConditionChips = (conditions:any) => {
        if (!conditions) return null;
        return conditions.map((code:any, index:any) => (
            <Chip key={index} label={`Cond: ${code}`} variant="outlined" size="small" style={{ marginLeft: 4 }} />
        ));
    };

    return (
      <div style={{ margin: 1, padding: '2em', minHeight: 'calc(100vh - 48px)'}}>

        <List dense>
          {tradeEvents.map((event: TradeEvent, index: number) => (
              <ListItem key={index} divider>
		<Card sx={{ width: '100%' }}>
                  <CardContent>
                    <Grid container alignItems="center" spacing={2}>
                      <Grid item>
			<Typography variant="h6" color="textPrimary">
                          {event.sym}
			</Typography>
                      </Grid>
                      <Grid item>
			<Typography variant="body1">
                          ${event.p.toFixed(2)}
			</Typography>
                      </Grid>
                      <Grid item>
			<Typography variant="body1">
                          Size: {event.s}
			</Typography>
                      </Grid>
                      <Grid item>
			<Typography variant="body1">
                          Exch: {event.x}
			</Typography>
                      </Grid>
                      <Grid item>
			<Typography variant="body2" color="textSecondary">
                          {new Date(event.t).toLocaleTimeString()}
			</Typography>
                      </Grid>
                      <Grid item>
			{event.c && getConditionChips(event.c)}
                      </Grid>
                    </Grid>
                  </CardContent>
		</Card>
              </ListItem>
          ))}
        </List>

      </div>
    );
};

export default LiveTrades;
