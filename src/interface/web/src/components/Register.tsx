import React, { useState } from 'react';
import { useAuth } from '../contexts/AuthContext';
import { Button, TextField, Box, Typography, Paper, Alert } from '@mui/material';


export const Register = () => {
    const { register, isAuthenticated } = useAuth();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleRegister = async () => {
        
        await register(username, password);
          
    };

    return (
        <Paper style={{ padding: '20px', margin: '16px' }}>
            <Typography variant="h5" gutterBottom>Register</Typography>
            <Box component="form" noValidate autoComplete="off" sx={{ '& > :not(style)': { m: 1 } }}>
                <TextField
                    fullWidth
                    label="Username"
                    variant="outlined"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
                <TextField
                    fullWidth
                    label="Password"
                    type="password"
                    variant="outlined"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <Button variant="contained" color="primary" onClick={handleRegister}>
                    Register
                </Button>
                {error && <Alert severity="error">{error}</Alert>}
            </Box>
        </Paper>
    );
};
