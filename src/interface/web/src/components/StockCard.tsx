import React from 'react';
import { Card, CardContent, Typography, Button, CardActions, Grid } from '@mui/material';


interface StockCardProps {
  symbol: string;
    price: number;
    subscribed: boolean;
    onToggleSubscription: (symbol: string, subscribed: boolean) => void;
}

const StockCard: React.FC<StockCardProps> = ({ symbol, price, subscribed, onToggleSubscription }) => {
    return (
      <Card variant="outlined" sx={{ minWidth: 275, margin: 2 }} >
        <CardContent>
          <Grid container alignItems="center" justifyContent="space-between">
            <Grid item xs={6}>
              <Typography variant="h5" component="div" sx={{ fontWeight: 'bold' }}>
                {symbol}
              </Typography>
            </Grid>
            <Grid item xs={6} sx={{ textAlign: 'right' }}>
                <Typography variant="body1" color="text.secondary">
                  ${price.toFixed(2)}
                </Typography>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions sx={{ justifyContent: 'flex-end', paddingRight: 2, minHeight: '52px' }}>
          <Button
            size="small"
            variant="contained"
            color={subscribed ? "secondary" : "primary"}
            onClick={() => onToggleSubscription(symbol, !subscribed)}
            sx={{ minWidth: '90px' }} // This ensures the button width remains constant
          >
            {subscribed ? 'Unsubscribe' : 'Subscribe'}
          </Button>
        </CardActions>
      </Card>
    );
};

export default StockCard;
