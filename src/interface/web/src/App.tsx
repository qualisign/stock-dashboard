// src/App.js
import React from 'react';
import { WebSocketProvider } from './contexts/WebSocketContext';
import StockDashboard from './components/StockDashboard';
import { AuthProvider } from './contexts/AuthContext';
const port = process.env.REACT_APP_WS_PORT;

const App = () => {
    const webSocketUrl = `ws://localhost:${port}`; // Your WebSocket connection URL

  return (
    <AuthProvider>
        <WebSocketProvider url={webSocketUrl}>
            <StockDashboard />
        </WebSocketProvider>
    </AuthProvider>
    );
};

export default App;
