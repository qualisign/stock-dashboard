import React, { createContext, useContext, useState, useEffect } from 'react';
import { User } from "../../../../domain/models/user";
const port = process.env.REACT_APP_WS_PORT;

interface AuthContextType {
  isAuthenticated: boolean;
  user: User | null;
  token: string | null;
  login: (username: string, password: string) => Promise<void>;
  logout: () => void;
  register: (username: string, password: string) => Promise<void>;
}

// Create a default state
const defaultAuthState: AuthContextType = {
  isAuthenticated: false,
  user: null,
  token: null,
  login: async () => {}, 
  logout: () => {},
  register: async () => {}, 
};

export const AuthContext = createContext<AuthContextType>(defaultAuthState);

interface AuthProviderProps {
  children: React.ReactNode;
}

function parseJwt(token:string) {
    try {
        return JSON.parse(atob(token.split('.')[1]));
    } catch (e) {
        return null;
    }
}




export const AuthProvider = ({ children }: AuthProviderProps) => {
  const [user, setUser] = useState<User | null>(null);
  const [token, setToken] = useState<string | null>(null);

  const login = async (username: string, password: string) => {
    console.log('logging in ' + username)    
    try {
      const response = await fetch(`http://localhost:${port}/api/users/login`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
      });
      console.log(response);
      const data = await response.json();
      console.log(data);
      if (response.ok) {
	console.log(data)

	const decoded = parseJwt(data.token);
	console.log(decoded);
	
        setToken(data.token);
	
        setUser({passwordHash: '', email: '', ...decoded});
      } else {
        throw new Error(data.message);
      }
    } catch (error) {
      console.error('Login failed:', error);
    }
  };

  const register = async (username: string, password: string) => {
    console.log('registering ' + username)
    try {
      const response = await fetch(`http://localhost:${port}/api/users/register`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
      });
      const data = await response.json();
      if (response.ok) {
	console.log(response);
      } else {
	console.log(data.message)
	console.log('error')
        throw new Error(data.message);
      }
    } catch (error) {
      console.error('Login failed:', error);
    }
  };


  const logout = () => {
    setUser(null);
    setToken(null);
  };

  return (
    <AuthContext.Provider value={{ isAuthenticated: !!user, user, token, login, logout, register }}>
      {children}
    </AuthContext.Provider>
  );
};


export const useAuth = () => useContext(AuthContext);
