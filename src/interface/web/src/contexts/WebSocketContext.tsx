import React, { createContext, useContext, useEffect, useState } from 'react';
import io, { Socket } from 'socket.io-client';
import axios from 'axios';
import { TradeEvent } from "../../../../domain/models/tradeEvent";
import { Stock } from "../../../../domain/models/stock";
import { useAuth } from './AuthContext';

interface WebSocketContextType {
    stocks: Stock[];
    tradeEvents: TradeEvent[];
    sendSubscriptionUpdate: (symbol: string, action: 'subscribe' | 'unsubscribe') => void;
}

export const WebSocketContext = createContext<WebSocketContextType>({
    stocks: [],
    tradeEvents: [],
    sendSubscriptionUpdate: () => {} // Dummy function for initial context value
});

export const useWebSocket = () => useContext(WebSocketContext);

export const WebSocketProvider: React.FC<{ children: React.ReactNode; url: string }> = ({ children, url }) => {
    const { token } = useAuth();
    const [stocks, setStocks] = useState<Stock[]>([]);
    const [tradeEvents, setTradeEvents] = useState<TradeEvent[]>([]);
    const [socket, setSocket] = useState<Socket | null>(null);

    // Fetch initial stocks on mount and whenever the token changes
    useEffect(() => {
        const fetchInitialStocks = async () => {
            try {
                const response = await axios.get<Stock[]>('http://localhost:3000/api/stocks', {
                    headers: { Authorization: `Bearer ${token}` }
                });
                setStocks(response.data);
                console.log('Initial stocks fetched', response.data);
            } catch (error) {
                console.error('Failed to fetch initial stocks:', error);
            }
        };

        if (token) {
            fetchInitialStocks();
        }
    }, [token]);

    // Setup WebSocket connection
    useEffect(() => {
        if (!token) {
            console.error('No token available for WebSocket connection');
            return;
        }

        const newSocket = io(url, {
            transports: ['websocket'],
            query: { token }
        });

        newSocket.on('connect', () => console.log("WebSocket connection established"));
        newSocket.on('tradeEvent', (event: TradeEvent) => {
	        setTradeEvents(prevEvents => {
        const newEvents = [event, ...prevEvents];
        if (newEvents.length > 100) newEvents.length = 100; // Keep only the last 100 events
        return newEvents;
    });
            setStocks(prevStocks => {
                const index = prevStocks.findIndex(stock => stock.symbol === event.sym);
                if (index !== -1) {
                    const updatedStocks = [...prevStocks];
                    updatedStocks[index] = {...updatedStocks[index], price: event.p};
                    return updatedStocks;
                }
                return prevStocks;
            });
        });

        newSocket.on('connect_error', error => console.error('Connection Error:', error));
        newSocket.on('disconnect', () => console.log("WebSocket connection closed"));
        newSocket.on('error', error => console.error('Error:', error));

        setSocket(newSocket);

        return () => {
            newSocket.disconnect();
        };
    }, [url, token]);

    const sendSubscriptionUpdate = (symbol: string, action: 'subscribe' | 'unsubscribe') => {
        if (socket) {
            socket.emit('subscriptionUpdate', { symbol, action });
        }
    };

    return (
        <WebSocketContext.Provider value={{ stocks, tradeEvents, sendSubscriptionUpdate }}>
            {children}
        </WebSocketContext.Provider>
    );
};
