import dotenv from 'dotenv';
dotenv.config({ path: '../.env' });

export const config = {
  jwtSecret: process.env.JWT_SECRET || 'secret', // Fallback to default if not specified
};

