import { Stock, StockSymbol } from "../../../domain/models/stock";
import { IStockRepository } from "./IStockRepository";

export class InMemoryStockRepository implements IStockRepository {
    private stocks: Record<StockSymbol, number> = {}; // Properly declare the stocks property

    async upsertStock(symbol: StockSymbol, price: number): Promise<void> {
        this.stocks[symbol] = price;
        console.log(`Stock upserted: ${symbol} at price ${price}`);
    }

    async getStock(symbol: StockSymbol): Promise<Stock | null> {
        const price = this.stocks[symbol];
        if (price !== undefined) {
            console.log(`Stock retrieved: ${symbol} at price ${price}`);
            return { symbol, price }; // Ensure return type matches Promise<Stock | null>
        } else {
            console.error(`Stock not found: ${symbol}`);
            return null;
        }
    }

    async listStocks(): Promise<Stock[]> {
        const stockList = Object.keys(this.stocks).map(symbol => ({
            symbol: symbol as StockSymbol,
            price: this.stocks[symbol]
        }));
        console.log('Listing all stocks:', stockList);
        return stockList; // Ensure return type matches Promise<Stock[]>
    }

    async deleteStock(symbol: StockSymbol): Promise<void> {
        if (this.stocks.hasOwnProperty(symbol)) {
            delete this.stocks[symbol];
            console.log(`Stock deleted: ${symbol}`);
        } else {
            console.error(`Stock not found: ${symbol}`);
        }
    }
}
