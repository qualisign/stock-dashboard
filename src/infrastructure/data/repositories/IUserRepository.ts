import { User } from '../../../domain/models/user';
export interface IUserRepository {
    findByUsername(username: string): Promise<User | null>;
    findById(userId: string): Promise<User | null>;  // Add this method
    createUser(user: User): Promise<string>;
    updateUser(userId: string, user: User): Promise<void>;
    deleteUser(userId: string): Promise<void>;
    validateCredentials(username: string, password: string): Promise<boolean>;
}
