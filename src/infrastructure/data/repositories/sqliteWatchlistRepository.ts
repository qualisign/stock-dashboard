import sqlite3 from 'sqlite3';
import { promisify } from 'util';
import path from 'path';

export class SqliteWatchlistRepository {
    private db: sqlite3.Database;

    constructor(databasePath: string) {
        this.db = new sqlite3.Database(databasePath, async (err) => {
            if (err) {
                console.error('Error opening database', err);
                return;
            }
            console.log('Database opened successfully');
            await this.initializeDatabase();
        });
    }

    private async initializeDatabase(): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run(`
                CREATE TABLE IF NOT EXISTS watchlists (
                    userId TEXT PRIMARY KEY,
                    symbols TEXT
                );
            `);
            console.log('Watchlist table is created or already exists');
        } catch (err) {
            console.error('Error creating watchlist table', err);
        }
    }

    async getWatchlistByUserId(userId: string): Promise<string[]> {
        const get = promisify(this.db.get.bind(this.db));
        try {
            const row = await get("SELECT symbols FROM watchlists WHERE userId = ?", userId);
            return row ? JSON.parse(row.symbols) : [];
        } catch (err) {
            console.error('Error fetching watchlist', err);
            throw err;
        }
    }

    async upsertWatchlist(userId: string, symbols: string[]): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run(`
                INSERT INTO watchlists (userId, symbols)
                VALUES (?, ?)
                ON CONFLICT(userId) DO UPDATE SET symbols = excluded.symbols;
            `, [userId, JSON.stringify(symbols)]);
            console.log('Watchlist upserted successfully for user:', userId);
        } catch (err) {
            console.error('Error upserting watchlist', err);
            throw err;
        }
    }

    async deleteWatchlistByUserId(userId: string): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run("DELETE FROM watchlists WHERE userId = ?", userId);
            console.log('Watchlist deleted successfully for user:', userId);
        } catch (err) {
            console.error('Error deleting watchlist', err);
            throw err;
        }
    }

    async closeConnection(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.close((err) => {
                if (err) reject(err);
                else {
                    console.log('Database connection closed');
                    resolve();
                }
            });
        });
    }
}
