import { DailyStats } from "../../../domain/models/dailyStats";

export interface IDailyStatsRepository {
    /**
     * Retrieves daily statistics for a specific stock symbol.
     * @param symbol The stock symbol for which daily statistics are requested.
     * @returns A promise that resolves to the daily stats object or null if not found.
     */
    getDailyStats(symbol: string): Promise<DailyStats | null>;
    listDailyStats(): Promise<DailyStats[]>;

    /**
     * Updates daily statistics for a specific stock symbol. If the entry does not exist, it creates a new one.
     * @param stats The daily stats data transfer object containing all necessary stats data.
     * @returns A promise that resolves when the operation is complete.
     */
    upsertDailyStats(stats: DailyStats): Promise<void>;
}
