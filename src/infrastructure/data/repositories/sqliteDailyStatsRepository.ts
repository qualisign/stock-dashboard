import sqlite3 from 'sqlite3';
import { IDailyStatsRepository } from './IDailyStatsRepository';
import { DailyStats } from '../../../domain/models/dailyStats';

export class SqliteDailyStatsRepository implements IDailyStatsRepository {

    private db: sqlite3.Database;

    constructor(databasePath: string) {
        this.db = new sqlite3.Database(databasePath, (err) => {
            if (err) {
                console.error('Error opening database', err);
                throw err;
            }
        });
    }

    async setupDatabase(): Promise<void> {
        const query = `
            CREATE TABLE IF NOT EXISTS dailyStats (
                symbol TEXT PRIMARY KEY,
                high REAL,
                highDateTime TEXT,
                low REAL,
                lowDateTime TEXT,
                closing REAL
            );
        `;
        return new Promise<void>((resolve, reject) => {
            this.db.run(query, (err) => {
                if (err) {
                    console.error('Error executing SQL to create dailyStats table', err);
                    reject(err);
                } else {
                    console.log('dailyStats table created or already exists');
                    resolve();
                }
            });
        });
    }


async listDailyStats(): Promise<DailyStats[]> {
    return new Promise((resolve, reject) => {
        this.db.all('SELECT * FROM dailyStats', [], (err, rows: any[]) => {
            if (err) {
                console.error('Error querying all dailyStats', err);
                reject(err);
            } else {
                resolve(rows.map(row => ({
                    symbol: row.symbol,
                    high: row.high,
                    highDateTime: row.highDateTime ? new Date(row.highDateTime) : null,
                    low: row.low,
                    lowDateTime: row.lowDateTime ? new Date(row.lowDateTime) : null,
                    closing: row.closing
                })));
            }
        });
    });
}

async getDailyStats(symbol: string): Promise<DailyStats | null> {
    return new Promise((resolve, reject) => {
        this.db.get('SELECT * FROM dailyStats WHERE symbol = ?', [symbol], (err, row: any) => {
            if (err) {
                console.error('Error querying dailyStats', err);
                reject(err);
            } else if (row) {
                resolve({
                    symbol: row.symbol,
                    high: row.high,
                    highDateTime: row.highDateTime ? new Date(row.highDateTime) : null,
                    low: row.low,
                    lowDateTime: row.lowDateTime ? new Date(row.lowDateTime) : null,
                    closing: row.closing
                });
            } else {
                resolve(null);
            }
        });
    });
}

async upsertDailyStats(stats: DailyStats): Promise<void> {
    const query = `
        INSERT INTO dailyStats (symbol, high, highDateTime, low, lowDateTime, closing)
        VALUES (?, ?, ?, ?, ?, ?)
        ON CONFLICT(symbol) DO UPDATE SET
            high = EXCLUDED.high,
            highDateTime = EXCLUDED.highDateTime,
            low = EXCLUDED.low,
            lowDateTime = EXCLUDED.lowDateTime,
            closing = EXCLUDED.closing;
    `;
    return new Promise<void>((resolve, reject) => {
        this.db.run(query, [
            stats.symbol,
            stats.high,
            stats.highDateTime.toISOString(), // Ensure the date is in ISO format
            stats.low,
            stats.lowDateTime.toISOString(), // Ensure the date is in ISO format
            stats.closing
        ], (err) => {
            if (err) {
                console.error(`Error upserting data for ${stats.symbol}`, err);
                reject(err);
            } else {
                resolve();
            }
        });
    });
}


  async deleteDailyStats(symbol: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
            this.db.run('DELETE FROM dailyStats WHERE symbol = ?', [symbol], (err) => {
                if (err) {
                    console.error(`Error deleting stats for symbol ${symbol}`, err);
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    close(): void {
        this.db.close((err) => {
            if (err) {
                console.error('Failed to close the database', err);
            } else {
                console.log('Database connection closed');
            }
        });
    }
}
