import { Stock } from "../../../domain/models/stock";

export interface IStockRepository {
    upsertStock(symbol: string, price: number): Promise<void>;
    deleteStock(symbol: string): Promise<void>;
    getStock(symbol: string): Promise<Stock | null>;
    listStocks(): Promise<Stock[]>;
}
