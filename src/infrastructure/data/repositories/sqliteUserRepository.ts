import sqlite3 from 'sqlite3';
import { promisify } from 'util';
import { IUserRepository } from './IUserRepository';
import { User } from '../../../domain/models/user';

export class SqliteUserRepository implements IUserRepository {
    private db: sqlite3.Database;

    constructor(databasePath: string) {
        this.db = new sqlite3.Database(databasePath, (err) => {
            if (err) {
                console.error('Error opening database', err);
            } else {
                console.log('Database connection successfully established.');
                this.setupDatabase().then(() => {
                    console.log('Users table created or verified successfully');
                }).catch(err => {
                    console.error('Error setting up the users table', err);
                });
            }
        });
    }

    private async setupDatabase(): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run(`
                CREATE TABLE IF NOT EXISTS users (
                    id TEXT PRIMARY KEY,
                    username TEXT UNIQUE,
                    passwordHash TEXT,
                    email TEXT
                );
            `);
        } catch (err) {
            console.error('Error setting up the users table', err);
            throw err;
        }
    }

    async findByUsername(username: string): Promise<User | null> {
        const get = promisify(this.db.get.bind(this.db));
        try {
            const row = await get('SELECT * FROM users WHERE username = ?', username);
            return row || null;
        } catch (err) {
            console.error('Error finding user by username', err);
            throw err;
        }
    }

    async findById(userId: string): Promise<User | null> {
        const get = promisify(this.db.get.bind(this.db));
        try {
            const row = await get('SELECT * FROM users WHERE id = ?', userId);
            return row ? { id: row.id, username: row.username, email: row.email, passwordHash: ''} : null;
        } catch (err) {
            console.error('Error finding user by ID', err);
            throw err;
        }
    }

    async createUser(user: User): Promise<string> {
	console.log(user)
        const run = promisify(this.db.run.bind(this.db));
        try {
            const result = await run('INSERT INTO users (id, username, passwordHash, email) VALUES (?, ?, ?, ?)', [user.id, user.username, user.passwordHash, user.email]);
	    console.log(result)
            return result.lastID.toString();
        } catch (err) {
            console.error('Error creating user', err);
            throw err;
        }
    }

    async updateUser(userId: string, user: User): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run('UPDATE users SET username = ?, passwordHash = ?, email = ? WHERE id = ?', [user.username, user.passwordHash, user.email, userId]);
        } catch (err) {
            console.error('Error updating user', err);
            throw err;
        }
    }

    async deleteUser(userId: string): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run('DELETE FROM users WHERE id = ?', [userId]);
        } catch (err) {
            console.error('Error deleting user', err);
            throw err;
        }
    }

    async validateCredentials(username: string, passwordHash: string): Promise<boolean> {
        const get = promisify(this.db.get.bind(this.db));
        try {
            const row = await get('SELECT passwordHash FROM users WHERE username = ?', [username]);
            return row && row.passwordHash === passwordHash;
        } catch (err) {
            console.error('Error validating user credentials', err);
            throw err;
        }
    }
}
