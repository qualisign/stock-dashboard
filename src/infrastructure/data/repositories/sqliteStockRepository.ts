import sqlite3 from 'sqlite3';
import { promisify } from 'util';
import path from 'path';

export class SqliteStockRepository {
    private db: sqlite3.Database;

    constructor(databasePath: string) {
        this.db = new sqlite3.Database(databasePath, async (err) => {
            if (err) {
                console.error('Error opening database', err);
                return;
            }
            console.log('Database opened successfully');
            await this.initializeDatabase();
        });
    }

       async deleteStock(symbol: string): Promise<void> {
	   
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run("DELETE FROM stocks WHERE symbol = ?", [symbol]);
            console.log('Stock deleted successfully:', symbol);
        } catch (err) {
            console.error('Error deleting stock:', err);
            throw err;
        } finally {
            this.db.close();
        }
    }

    private async initializeDatabase(): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run(`
                CREATE TABLE IF NOT EXISTS stocks (
                    symbol TEXT PRIMARY KEY,
                    price REAL
                );
            `);
            console.log('Table is created or already exists');
        } catch (err) {
            console.error('Error creating table', err);
        }
    }

    async upsertStock(symbol: string, price: number): Promise<void> {
        const run = promisify(this.db.run.bind(this.db));
        try {
            await run(`INSERT INTO stocks (symbol, price) VALUES (?, ?)
                       ON CONFLICT(symbol) DO UPDATE SET price = excluded.price;`, [symbol, price]);
            console.log('Stock upserted successfully:', { symbol, price });
        } catch (err) {
            console.error('Error during upsert query:', err);
            throw err;
        }
    }

    async getStock(symbol: string): Promise<{ symbol: string, price: number } | null> {
        const get = promisify(this.db.get.bind(this.db));
        try {
            const row = await get("SELECT * FROM stocks WHERE symbol = ?", symbol);
            return row ? { price: row.price, symbol: row.symbol } : null;
        } catch (err) {
            console.error('Error retrieving stock:', err);
            throw err;
        }
    }

    async listStocks(): Promise<{ symbol: string, price: number }[]> {
        const all = promisify(this.db.all.bind(this.db));
        try {
            const rows = await all("SELECT * FROM stocks");
            return rows.map(row => ({ price: row.price, symbol: row.symbol }));
        } catch (err) {
            console.error('Error querying stocks:', err);
            throw err;
        }
    }

    async closeConnection(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.close((err) => {
                if (err) reject(err);
                else {
                    console.log('Database connection closed');
                    resolve();
                }
            });
        });
    }
}
