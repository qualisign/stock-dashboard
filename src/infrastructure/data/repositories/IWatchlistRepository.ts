export interface IWatchlistRepository {
    getWatchlistByUserId(userId: string): Promise<string[]>;
    upsertWatchlist(userId: string, symbols: string[]): Promise<void>;
    deleteWatchlistByUserId(userId: string): Promise<void>;
}
