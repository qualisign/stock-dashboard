// src/domain/factories/tradeEventFactory.ts
import { TradeEvent } from "../../../domain/models/tradeEvent"
import { stockSymbols } from "../../../domain/models/stock"

export class TradeEventFactory {
  static createTradeEvents(count: number = 10): TradeEvent[] {
    return Array.from({ length: count }, () => ({
      ev: 'T',
      sym: stockSymbols[Math.floor(Math.random() * stockSymbols.length)],
      i: Math.random().toString(36).substring(7),
      x: Math.floor(Math.random() * 100),
      p: parseFloat((Math.random() * 1000 + 1).toFixed(2)),
      s: Math.floor(Math.random() * 1000 + 1),
      t: Date.now(),
      z: Math.floor(Math.random() * 10),
      c: [Math.floor(Math.random() * 10)]
    }));
  }
}
