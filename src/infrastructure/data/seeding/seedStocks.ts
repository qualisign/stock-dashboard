import { createStocks } from './stockFactory';
import { InMemoryStockRepository } from '../repositories/inMemoryStockRepository';

async function seedStocks() {
  const repository = new InMemoryStockRepository();
  const stocks = createStocks();

  for (const stock of stocks) {
      await repository.upsertStock(stock.symbol, stock.price);
      console.log(await repository.getStock(stock.symbol));
      
  }
  console.log(await repository.listStocks());
  console.log('Stock data seeded successfully.');
}

seedStocks().catch(console.error);
