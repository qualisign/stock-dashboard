import { stockSymbols, Stock, StockSymbol } from "../../../domain/models/stock";

export function createStocks(): Stock[] {
  return stockSymbols.map((symbol: StockSymbol) => ({
    symbol,
    price: parseFloat((Math.random() * (1000 - 100) + 100).toFixed(2))
  }));
}


