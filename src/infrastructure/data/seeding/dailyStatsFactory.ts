import { DailyStats } from "../../../domain/models/dailyStats";
import { stockSymbols } from "../../../domain/models/stock";

export function createDailyStats(days: number = 7): DailyStats[] {
    const stats = [];

    for (let day = 0; day < days; day++) {
        const dayOffset = day * 24 * 60 * 60 * 1000; // days converted to milliseconds

        stockSymbols.forEach(symbol => {
            const highDateSubtract = Math.floor(Math.random() * 3600000);
            const lowDateSubtract = Math.floor(Math.random() * 3600000);

            const highDateTime = new Date(Date.now() - dayOffset - highDateSubtract);
            const lowDateTime = new Date(Date.now() - dayOffset - lowDateSubtract);

            // Logging the dates and subtracted values for debugging
            console.log(`Symbol: ${symbol}, High Date Time: ${highDateTime}, High Subtract: ${highDateSubtract}, Low Date Time: ${lowDateTime}, Low Subtract: ${lowDateSubtract}`);

            stats.push({
                symbol: symbol,
                high: parseFloat((Math.random() * 1000 + 100).toFixed(2)),
                low: parseFloat((Math.random() * 100).toFixed(2)),
                highDateTime: highDateTime,
                lowDateTime: lowDateTime,
                closing: parseFloat((Math.random() * 1000 + 50).toFixed(2)),
            });
        });
    }

    return stats;
}
