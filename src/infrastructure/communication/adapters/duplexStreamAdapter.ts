
import { Server as SocketIOServer } from "socket.io";
import { EventEmitter } from 'events';
import * as http from 'http';
import { IDuplexStreamAdapter, IAuthService, IReadStreamAdapter } from "../../../domain/interfaces";

export class DuplexStreamAdapter extends EventEmitter implements IDuplexStreamAdapter {
  private io: SocketIOServer;
  private readStreamAdapter: IReadStreamAdapter;
  private authService: IAuthService;


constructor(readStreamAdapter: IReadStreamAdapter, httpServer: http.Server, authService: IAuthService) {
    super();
    this.authService = authService;
    this.readStreamAdapter = readStreamAdapter;
    this.io = new SocketIOServer(httpServer, { cors: { origin: '*' } });

    console.log("Initializing WebSocket Server");
    this.initializeWebSocketServer();
    console.log("Setting up Data Stream Listeners");
    this.setupReadStreamListeners();
}

public initializeWebSocketServer(): void {
    this.io.on('connection', async (socket) => {
        console.log("Socket connection attempt");
        const token = socket.handshake.query.token;
        console.log(`Token received: ${token}`);
        if (!token) {
            console.log("No token provided, disconnecting socket.");
            socket.disconnect(true);
            return;
        }
        const user = await this.authService.verifyToken(token.toString());
        console.log(`User verification result: ${user ? 'Verified' : 'Not Verified'}`);
        if (!user) {
            console.log("Invalid or expired token, disconnecting.");
            socket.disconnect(true);
            return;
        }
        console.log(`User connected: ${user.id}`);
        this.emit('clientConnected', { socket, userId: user.id });


	socket.on('subscriptionUpdate', ({ symbol, action }) => {
            console.log(`Subscription update from ${user.id}: ${action} ${symbol}`);
            if (action === 'subscribe') {
                // Add logic to handle subscription
                this.emit('subscribe', user.id, symbol);
            } else if (action === 'unsubscribe') {
                // Add logic to handle unsubscription
                this.emit('unsubscribe', user.id, symbol);		
            }
        });
	
    });
}    

  private setupReadStreamListeners(): void {
      this.readStreamAdapter.on('data', (data) => {
	  
        this.emit('tradeEvent', data);
    });

    this.readStreamAdapter.on('error', (error) => {
        console.error('Error from read stream:', error);
    });

    this.readStreamAdapter.on('closed', () => {
        console.log('ReadStream closed');
    });
  }

  public stopService(): void {
    if (this.io) {
      this.io.close(() => console.log("WebSocket server stopped."));
    }
    this.readStreamAdapter.closeDataStream();
  }
}
