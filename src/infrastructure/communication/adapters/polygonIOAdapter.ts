// src/infrastructure/communication/adapters/PolygonIOAdapter.ts

import { IReadStreamAdapter } from "../../../domain/interfaces";
import { EventEmitter } from 'events';
import { websocketClient } from "@polygon.io/client-js";

export class PolygonIOAdapter extends EventEmitter implements IReadStreamAdapter {
    private polygonWS: WebSocket;  // Assume WebSocket here refers to a standard WebSocket interface

    constructor(private polygonAPIKey: string) {
        super();
        this.initializeDataStream();
    }

    public initializeDataStream(): void {
        // Assume websocketClient creates a compatible WebSocket connection
        this.polygonWS = websocketClient(this.polygonAPIKey).stocks() as unknown as WebSocket;

        this.polygonWS.addEventListener('open', () => {
            console.log("Polygon WebSocket connection established.");
            this.polygonWS.send(JSON.stringify({ action: "auth", params: this.polygonAPIKey }));
            this.polygonWS.send(JSON.stringify({ action: "subscribe", params: "T.MSFT" }));
        });

        this.polygonWS.addEventListener('message', event => {
            console.log(`Received data from Polygon: ${event.data}`);
            const message = JSON.parse(event.data);
            this.emit('data', message);  // Directly emit data to listeners
        });

        this.polygonWS.addEventListener('error', event => {
            console.error('WebSocket error from Polygon API:', event);
            this.emit('error', event);
        });

        this.polygonWS.addEventListener('close', () => {
            console.log('Polygon WebSocket client disconnected:');
            this.emit('closed');
        });
    }

    public closeDataStream(): void {
        if (this.polygonWS) {
            this.polygonWS.close();
        }
    }
}
