// src/infrastructure/communication/adapters/MockReadStreamAdapter.ts
import { EventEmitter } from "events";
import { IReadStreamAdapter } from "../../../domain/interfaces";
import { TradeEventFactory } from "../../data/seeding/tradeEventFactory";

export class MockReadStreamAdapter extends EventEmitter implements IReadStreamAdapter {
  private intervalId: NodeJS.Timeout | null = null;

  constructor() {
      super();
      this.initializeDataStream()
  }

  initializeDataStream(): void {
    // Initialize the data stream here, starts emitting events at intervals
    this.intervalId = setInterval(() => this.emitTradeEvents(), 1000);
    console.log("DataStream initialized and events will be emitted every 1 second.");
  }

    emitTradeEvents() {
    const tradeEvents = TradeEventFactory.createTradeEvents();
    console.log(`Emitting trade events: ${JSON.stringify(tradeEvents)}`);
    this.emit('data', tradeEvents);
}


  closeDataStream(): void {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
    this.removeAllListeners();
    console.log("MockReadStream closed.");
  }
}
